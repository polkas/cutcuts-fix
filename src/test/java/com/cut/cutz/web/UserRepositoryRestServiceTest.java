package com.cut.cutz.web;

import com.cut.cutz.db.base.dbunit.UserRepositoryBase;
import org.junit.Test;
import org.springframework.http.MediaType;

import static com.cut.cutz.constants.RestConstants.*;
import static com.cut.cutz.constants.TestConstants.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Pogorelov on 12.09.2015
 */
public class UserRepositoryRestServiceTest extends UserRepositoryBase {

    @Test
    public void loginNewUser() throws Exception {
        mockMvc.perform(get(API_URL + REST_URL + LOGIN_URL)
                .param(EMAIL_PARAM, NEW_USER_EMAIL)
                .param(PASSWORD_PARAM, NEW_USER_PASSWORD)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void loginExistsUser() throws Exception  {
        mockMvc.perform(get(API_URL + REST_URL + LOGIN_URL)
                .param(EMAIL_PARAM, EXISTS_USER_EMAIL)
                .param(PASSWORD_PARAM, EXISTS_USER_PASSWORD)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void notAbleToLoginUser() throws Exception  {
        mockMvc.perform(get(API_URL + REST_URL + LOGIN_URL)
                .param(EMAIL_PARAM, EXISTS_USER_EMAIL)
                .param(PASSWORD_PARAM, "incorrectPassword")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isConflict());
    }

    @Test
    public void recoverPasswordForUser() throws Exception  {
        mockMvc.perform(post(API_URL + REST_URL + RECOVER_PASSWORD_URL)
                .param(EMAIL_PARAM, EXISTS_USER_EMAIL)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

        emailTest();
    }

    @Test
    public void followUserByExistingUser() throws Exception  {
        mockMvc.perform(post(API_URL + REST_URL + USERS_URL + "/3" + FOLLOW_USER_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void unfollowUserByExistingUser() throws Exception  {
        mockMvc.perform(post(API_URL + REST_URL + USERS_URL + "/2" + FOLLOW_USER_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReceiveFollowers() throws Exception  {
        mockMvc.perform(get(API_URL + REST_URL + USERS_URL + "/2" + FOLLOWERS_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReceiveFollowing() throws Exception  {
        mockMvc.perform(get(API_URL + REST_URL + USERS_URL + "/2" + FOLLOWING_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }


    @Test
    public void getUser() throws Exception  {
        mockMvc.perform(get(API_URL + REST_URL + USERS_URL + "/2")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }
}
