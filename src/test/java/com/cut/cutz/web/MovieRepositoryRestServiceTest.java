package com.cut.cutz.web;

import com.cut.cutz.db.base.dbunit.MovieRepositoryBase;
import org.junit.Test;
import org.springframework.http.MediaType;

import static com.cut.cutz.constants.RestConstants.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Pogorelov on 12.09.2015
 */
public class MovieRepositoryRestServiceTest extends MovieRepositoryBase {

    @Test
    public void getMoviesByGenre() throws Exception  {
        mockMvc.perform(get(API_URL + REST_URL + MOVIE_URL + GENRE_URL + "/1")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void increaseView() throws Exception  {
        mockMvc.perform(post(API_URL + REST_URL + MOVIE_URL + "/5" + VIEWS_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void increaseLikes() throws Exception  {
        mockMvc.perform(post(API_URL + REST_URL + MOVIE_URL + "/7" + LIKES_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getPopularMovies() throws Exception  {
        mockMvc.perform(get(API_URL + REST_URL + MOVIE_URL + POPULAR_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getUserMovies() throws Exception  {
        mockMvc.perform(get(API_URL + REST_URL + MOVIE_URL + FEEDS_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }
}
