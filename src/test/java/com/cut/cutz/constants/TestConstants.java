package com.cut.cutz.constants;

/**
 * @author Pogorelov on 12.09.2015
 */
public class TestConstants {

    public static final String NEW_USER_EMAIL = "new.user@new.user.com";
    public static final String NEW_USER_PASSWORD = "new.user_password";

    public static final String EXISTS_USER_EMAIL = "test.user@test.user.com";
    public static final String EXISTS_USER_PASSWORD = "test.password";

    public static final Integer EXISTS_USER_ID = 4;
    public static final Long EXISTS_PARENT_MOVIE_5_ID = 5L;
    public static final Long EXISTS_MOVIE_8_ID = 8L;
    public static final Long EXISTS_MOVIE_ID = 7L;
}
