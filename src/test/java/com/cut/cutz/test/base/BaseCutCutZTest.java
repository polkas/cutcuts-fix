package com.cut.cutz.test.base;

import com.cut.cutz.CutCutZApplication;
import com.cut.cutz.constants.Constants;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.service.UserService;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

import static com.cut.cutz.constants.TestConstants.EXISTS_USER_EMAIL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Pogorelov on 10.09.2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CutCutZApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@DbUnitConfiguration(dataSetLoader = ColumnSensingXMLDataSetLoader.class)
@WebAppConfiguration
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
public abstract class BaseCutCutZTest {
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private UserService userService;

    private static final List<String> dropTableScripts = readDropScript();

    protected GreenMail greenMail;
    private static final String LOCALHOST = "localhost";


    protected static final MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    protected MockMvc mockMvc;


    @PostConstruct
    public void clearDatabase() throws IOException {
        String[] deleteTableScripts = new String[dropTableScripts.size() + 1];
        Iterator<String> iterator = dropTableScripts.iterator();
        for (int i=0; iterator.hasNext(); i++) {
            final String dropTableScript = iterator.next();
            if (!dropTableScript.contains("schema_version")) {
                deleteTableScripts[i] = dropTableScript.replace("DROP TABLE", "DELETE FROM");
            }
        }
        jdbcTemplate.batchUpdate(deleteTableScripts);
    }

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();

        ServerSetup setup = new ServerSetup(3025, LOCALHOST, "smtp");

        greenMail = new GreenMail(setup);
        greenMail.start();

        setMockDefaultUser();
    }

    @After
    public void tearDown() throws Exception {
        if(greenMail != null) greenMail.stop();
    }

    private static List<String> readDropScript() {
        try {
            return IOUtils.readLines(new ClassPathResource("db/custom/dropDB.sql").getInputStream());
        } catch (Exception e) {
            throw new RuntimeException("Something bad happend", e);
        }
    }

    protected void emailTest() throws MessagingException {
        assertTrue(greenMail.waitForIncomingEmail(10000, 1));

        MimeMessage[] messages = greenMail.getReceivedMessages();

        assertEquals(1, messages.length);
        assertEquals(Constants.EMAIL_USER_SUBJECT, messages[0].getSubject());
    }

    protected User getCurrentUser() {
        return userService.getCurrentUser();
    }

    protected void setMockCurrentUser(String email) {
        mockCurrentUser(email);
    }

    protected void setMockDefaultUser() {
        final String defaultMockUserEmail = EXISTS_USER_EMAIL;
        mockCurrentUser(defaultMockUserEmail);
    }

    private void mockCurrentUser(String email) {
        final User mockUser = userService.findByEmail(email);
        userService.setCurrentUser(mockUser, new MockHttpServletRequest());
    }
}
