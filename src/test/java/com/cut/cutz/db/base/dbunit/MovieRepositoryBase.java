package com.cut.cutz.db.base.dbunit;

import com.cut.cutz.test.base.BaseCutCutZTest;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * @author Pogorelov on 12.09.2015
 */

@DatabaseSetup("classpath:dbunit/MovieRepository/testData.xml")
public abstract class MovieRepositoryBase extends BaseCutCutZTest {
}
