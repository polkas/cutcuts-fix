package com.cut.cutz.db.repository;

import com.cut.cutz.db.base.dbunit.MovieRepositoryBase;
import com.cut.cutz.db.entity.Comment;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.exception.UnexpectedDataException;
import com.cut.cutz.service.CommentService;
import com.cut.cutz.service.MovieService;
import com.cut.cutz.web.dto.CommentDto;
import com.cut.cutz.web.dto.PreviewComponentsDto;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

import static com.cut.cutz.constants.ErrorConstants.*;
import static org.junit.Assert.*;

/**
 * @author Pogorelov on 1/2/2016
 */
public class CommentRepositoryTest extends MovieRepositoryBase {

    @Inject
    private CommentService commentService;
    @Inject
    private MovieService movieService;
    @Inject
    private CommentRepository commentRepository;

    @Test
    public void addNewComment() throws Exception {
        final String newComment = "New Comment!";
        final User user = getCurrentUser();
        final Long movieId = 8L;

        PreviewComponentsDto previewComponents = movieService.findPreviewComponents(movieId, user);
        List<CommentDto> commentsForMovieId = previewComponents.getComments();

        assertNotNull(commentsForMovieId);
        assertEquals(0, commentsForMovieId.size());

        CommentDto commentDto = commentService.addComment(user, movieId, newComment);

        assertNotNull(commentDto);
        assertEquals(newComment, commentDto.getText());
        assertEquals(user.getUsername(), commentDto.getAuthorName());
        assertEquals((Integer)6, commentDto.getAuthorMoviesAmount());
        assertEquals((Integer)0, commentDto.getAuthorFollowersAmount());

        previewComponents = movieService.findPreviewComponents(movieId, user);
        commentsForMovieId = previewComponents.getComments();

        assertNotNull(commentsForMovieId);
        assertEquals(1, commentsForMovieId.size());
        assertEquals(newComment, commentsForMovieId.get(0).getText());
    }

    @Test
    public void addNewCommentWithoutAuthor() throws Exception {
        final String newComment = "New Comment!";
        final User nullUser = null;
        final Long movieId = 6L;

        try {
            commentService.addComment(nullUser, movieId, newComment);
        } catch (UnexpectedDataException e) {
            assertEquals(UNEXPECTED_USER, e.getMessage());
        }
    }

    @Test
    public void addNewCommentWithoutMovie() throws Exception {
        final String newComment = "New Comment!";
        final User user = getCurrentUser();
        final Long nullMovieId = null;

        try {
            commentService.addComment(user, nullMovieId, newComment);
        } catch (UnexpectedDataException e) {
            assertEquals(UNEXPECTED_MOVIE, e.getMessage());
        }
    }

    @Test
    public void addNewCommentWithoutText() throws Exception {
        final String nullComment = null;
        final User user = getCurrentUser();
        final Long movieId = 6L;

        try {
            commentService.addComment(user, movieId, nullComment);
        } catch (UnexpectedDataException e) {
            assertEquals(COMMENT_REQUIRED, e.getMessage());
        }
    }


    @Test
    public void findExistingComments() {
        final Long movieId = 7L;

        PreviewComponentsDto previewComponents = movieService.findPreviewComponents(movieId, getCurrentUser());
        List<CommentDto> commentsForMovieId = previewComponents.getComments();
        assertNotNull(commentsForMovieId);
        assertEquals(2, commentsForMovieId.size());
        assertEquals("Hello world", commentsForMovieId.get(0).getText());
        assertEquals("2016-01-10 22:52:44.0", commentsForMovieId.get(0).getDate().toString());
        assertEquals("Hello world 2", commentsForMovieId.get(1).getText());
        assertEquals("2016-01-10 22:52:44.0", commentsForMovieId.get(1).getDate().toString());
    }

    @Test
    public void deleteExistingComment() {
        final Long ownCommentId = 1L;
        final Long movieId = 7L;
        Comment comment = commentRepository.findOne(ownCommentId);

        assertNotNull(comment);

        User user = getCurrentUser();
        commentService.removeComment(user, movieId, ownCommentId);

        comment = commentRepository.findOne(ownCommentId);
        assertNull(comment);
    }


    @Test
    public void cannotDeleteSomeoneElseComment() {
        final Long someoneElseCommentId = 2L;
        final Long movieId = 7L;
        Comment comment = commentRepository.findOne(someoneElseCommentId);

        assertNotNull(comment);

        User user = getCurrentUser();
        commentService.removeComment(user, movieId, someoneElseCommentId);

        comment = commentRepository.findOne(someoneElseCommentId);
        assertNotNull(comment);
    }

}
