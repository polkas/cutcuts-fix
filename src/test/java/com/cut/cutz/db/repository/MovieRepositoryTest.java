package com.cut.cutz.db.repository;

import com.cut.cutz.constants.ErrorConstants;
import com.cut.cutz.db.base.dbunit.MovieRepositoryBase;
import com.cut.cutz.db.entity.Comment;
import com.cut.cutz.db.entity.Movie;
import com.cut.cutz.db.entity.Segment;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.exception.UnexpectedDataException;
import com.cut.cutz.holder.SegmentsRecordHolder;
import com.cut.cutz.service.IntegrationService;
import com.cut.cutz.service.MovieService;
import com.cut.cutz.manager.MovieManager;
import com.cut.cutz.web.dto.LikeDto;
import com.cut.cutz.web.dto.MovieDto;
import com.cut.cutz.web.dto.PreviewComponentsDto;
import com.cut.cutz.web.dto.SegmentDto;
import com.cut.cutz.web.dto.base.BaseMovieDto;
import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.cut.cutz.constants.ErrorConstants.UNEXPECTED_MOVIE;
import static com.cut.cutz.constants.ErrorConstants.UNEXPECTED_USER;
import static com.cut.cutz.constants.TestConstants.*;
import static org.junit.Assert.*;

/**
 * @author Pogorelov on 09.09.2015
 */
public class MovieRepositoryTest extends MovieRepositoryBase {

    private static final Integer DURATION = 15;

    @Autowired
    private MovieService movieService;
    @Autowired
    private IntegrationService integrationService;
    @Autowired
    private MovieManager movieManager;

    @Test
    @Ignore
    public void localRecordMerger() throws Exception {
        List<SegmentsRecordHolder> list = new ArrayList<>();
        SegmentsRecordHolder segmentsRecordHolder = new SegmentsRecordHolder();
        byte[] recordInBytes = FileUtils.readFileToByteArray(new File("C:\\CutCutZ_Project\\audios\\someSound.m4a"));
        //segmentsRecordHolder.setStartTime(1000L).setEndTime(5000L).setRecordInBytes(recordInBytes);
        list.add(segmentsRecordHolder);
//
//        segmentsRecordHolder = new SegmentsRecordHolder();
//        recordInBytes = FileUtils.readFileToByteArray(new File("C:\\CutCutZ_Project\\audios\\glass_breaking.m4a"));
//        segmentsRecordHolder.setStartTime(10000L).setEndTime(14000L).setRecordInBytes(recordInBytes);
//        list.add(segmentsRecordHolder);
        final String st1 = "45db4683-9165-48fc-8127-0b874dbb3385.mp4";
        final String st2 = "or2.mp4";
        final String ss = "or2 - Copy.h264";
//        final String tes = "big_buck_bunny.mp4";
        movieManager.mergeVideoWithSound(st2, list);
    }

    @Test
    public void getParentMovie() {
        final Movie movie = movieService.getByMovieIdWithSegments(EXISTS_PARENT_MOVIE_5_ID);

        assertNotNull(movie);
        assertEquals("Movies Name", movie.getName());
        assertEquals("UserName", movie.getAuthor().getName());
        assertEquals("somePosterUrl", movie.getPosterUrl());
        assertEquals("someMovieUrl", movie.getMovieUrl());
        assertEquals(DURATION, movie.getDuration());

        List<Segment> segments = movie.getSegments();
        assertNotNull(segments);
        assertEquals(3, segments.size());
        assertEquals(new Long(1000), segments.get(0).getStartTime());
        assertEquals(new Long(5000), segments.get(0).getEndTime());
        assertEquals(new Long(7000), segments.get(1).getStartTime());
        assertEquals(new Long(10000), segments.get(1).getEndTime());
        assertEquals(new Long(12000), segments.get(2).getStartTime());
        assertEquals(new Long(15000), segments.get(2).getEndTime());

        List<Comment> comments = movie.getComments();
        assertNotNull(comments);
        assertEquals(0, comments.size());
    }

    @Test
    public void getCreatedMovie() {
        final Movie movie = movieService.getByMovieIdWithSegments(EXISTS_MOVIE_ID);

        assertNotNull(movie);
        assertEquals("Movies New", movie.getName());
        assertEquals("UserName", movie.getAuthor().getName());
        assertEquals("somePosterUrl", movie.getPosterUrl());
        assertEquals("someMovieUrl", movie.getMovieUrl());
        assertEquals(DURATION, movie.getDuration());

        final Movie parent = movie.getParent();
        assertNotNull(parent);
        assertEquals("Movies Name", parent.getName());
        assertEquals("somePosterUrl", parent.getPosterUrl());
        assertEquals("someMovieUrl", parent.getMovieUrl());
        assertEquals(DURATION, parent.getDuration());

        List<Segment> segments = movie.getSegments();
        assertNotNull(segments);
        assertEquals(0, segments.size());

        List<Comment> comments = movie.getComments();
        assertNotNull(comments);
        assertEquals(2, comments.size());
        assertEquals("Hello world", comments.get(0).getText());
        assertEquals("Hello world 2", comments.get(1).getText());
    }

    @Test
    public void getPreviewComponent() {
        final Long movieId = EXISTS_MOVIE_ID;
        PreviewComponentsDto previewComponents = movieService.findPreviewComponents(movieId, getCurrentUser());

        assertNotNull(previewComponents);

        List<MovieDto> similarMovies = previewComponents.getSimilarMovies();
        assertNotNull(similarMovies);
        assertEquals(2, similarMovies.size());
    }

    @Test
    public void findMovieWithoutMovieId() {
        final Long nullMovieId = null;
        try {
            movieService.getByMovieIdWithSegments(nullMovieId);
        } catch (UnexpectedDataException e) {
            assertEquals(UNEXPECTED_MOVIE, e.getMessage());
        }
    }

    @Test
    public void getMovieDtos() {
        User user = getCurrentUser();
        List<MovieDto> movieByUserDtos = movieService.getMoviesDtoByUser(user);

        assertNotNull(movieByUserDtos);
        assertEquals(4, movieByUserDtos.size());
    }

    @Test
    public void getMovieDtosWithoutUserId() {
        final User nullUser = null;
        try {
            movieService.getMoviesDtoByUser(nullUser);
        } catch (UnexpectedDataException e) {
            assertEquals(UNEXPECTED_USER, e.getMessage());
        }

    }

    @Test
    public void getPopularMovieDtos() {
        List<MovieDto> userMovieDtos = movieService.getPopularMoviesDto(getCurrentUser());

        assertNotNull(userMovieDtos);
        assertEquals(4, userMovieDtos.size());

        BaseMovieDto parentMovie = userMovieDtos.get(0).getParentMovie();
        assertNotNull(parentMovie);
    }

    @Test
    public void getNewestMovieDtos() {
        List<MovieDto> newsetMovies = movieService.getNewestMoviesDto(getCurrentUser());

        assertNotNull(newsetMovies);
        assertEquals(4, newsetMovies.size());

        BaseMovieDto parentMovie = newsetMovies.get(0).getParentMovie();
        assertNotNull(parentMovie);
    }

    @Test
    public void searchUsersWithLowercase() {
        List<MovieDto> userMovieDtos = movieService.searchMoviesByQuery("new", getCurrentUser());

        assertNotNull(userMovieDtos);
        assertEquals(2, userMovieDtos.size());
    }

    @Test
    public void getMovieDtosByGenre() {
        final int expectedSegmentsSize = 3;
        String actorNicknamePart = "actor";
        String actorUrlPart = "url";

        List<MovieDto> userMovieDtos = movieService.getMoviesDtoByGenreId(1, getCurrentUser());

        assertNotNull(userMovieDtos);
        assertEquals(1, userMovieDtos.size());
        assertNotNull(userMovieDtos.get(0));

        List<SegmentDto> segments = userMovieDtos.get(0).getSegments();
        assertNotNull(segments);
        assertEquals(expectedSegmentsSize, segments.size());

        SegmentDto segmentDto;
        for(int i = 0; i < expectedSegmentsSize; i++) {
            segmentDto = segments.get(i);
            assertNotNull(segmentDto);
            assertEquals(actorNicknamePart + (i + 1), segmentDto.getActor().getNickname());
            assertEquals(actorUrlPart + (i + 1), segmentDto.getActor().getPictureUrl());
        }
    }

    @Test
    public void getMovieDtosByGenreWithoutGenre() {
        final Integer nullGenreId = null;
        try {
            movieService.getMoviesDtoByGenreId(nullGenreId, getCurrentUser());
        } catch (UnexpectedDataException e) {
            assertEquals(ErrorConstants.UNEXPECTED_GENRE_ID, e.getMessage());
        }

    }

    @Test
    public void increaseViewsForMovie() {
        Movie movie = movieService.getByMovieIdWithSegments(EXISTS_PARENT_MOVIE_5_ID);
        Integer value = 0;
        assertNotNull(movie);
        assertEquals(value, movie.getViews());

        movieService.increaseMovieViews(EXISTS_PARENT_MOVIE_5_ID);
        ++value;

        //For increase method. It works asynchronously.
        try { Thread.sleep(1000); } catch (InterruptedException e) {}

        movie = movieService.getByMovieIdWithSegments(EXISTS_PARENT_MOVIE_5_ID);

        assertNotNull(movie);
        assertEquals(value, movie.getViews());
    }

    @Test
    public void increaseViewsWithoutMovie() {
        final Long nullMovieId = null;
        try {
            movieService.getByMovieIdWithSegments(nullMovieId);
        } catch (UnexpectedDataException e) {
            assertEquals(ErrorConstants.UNEXPECTED_MOVIE, e.getMessage());
        }
    }

    @Test
    public void increaseLikeForMovieByCurrentUser() {
        Movie movie = movieService.getByMovieIdWithSegments(EXISTS_MOVIE_8_ID);
        int likes = 3;
        assertNotNull(movie);
        assertNotNull(movie.getLikes());
        assertEquals(likes, movie.getLikes().size());


        LikeDto likeDto = integrationService.increaseMovieLikes(EXISTS_MOVIE_8_ID, getCurrentUser());
        ++likes;

        assertNotNull(likeDto);
        assertEquals((Integer)likes, likeDto.getLikes());
        assertTrue(likeDto.isLiked());

        movie = movieService.getByMovieIdWithSegments(EXISTS_MOVIE_8_ID);

        assertNotNull(movie);
        assertNotNull(movie.getLikes());
        assertEquals(likes, movie.getLikes().size());
    }

    @Test
    public void increaseLikeForMovieAndGetFeedByCurrentUser() {
        Movie movie = movieService.getByMovieIdWithSegments(EXISTS_MOVIE_8_ID);
        int likes = 3;
        assertNotNull(movie);
        assertNotNull(movie.getLikes());
        assertEquals(likes, movie.getLikes().size());


        LikeDto likeDto = integrationService.increaseMovieLikes(EXISTS_MOVIE_8_ID, getCurrentUser());
        ++likes;

        assertNotNull(likeDto);
        assertEquals((Integer)likes, likeDto.getLikes());
        assertTrue(likeDto.isLiked());

        movie = movieService.getByMovieIdWithSegments(EXISTS_MOVIE_8_ID);

        assertNotNull(movie);
        assertNotNull(movie.getLikes());
        assertEquals(likes, movie.getLikes().size());

        MovieDto movieDto = movieService.getByMovieDtoById(EXISTS_MOVIE_8_ID, getCurrentUser());
        assertNotNull(movieDto);
        assertNotNull(movieDto.getLikes());
        assertEquals((Integer)likes, movieDto.getLikes());
        assertTrue(movieDto.isLiked());
    }

    @Test
    public void decreaseLikeForMovieByCurrentUser() {
        Movie movie = movieService.getByMovieIdWithSegments(EXISTS_PARENT_MOVIE_5_ID);

        assertNotNull(movie);
        assertNotNull(movie.getLikes());

        int likes = movie.getLikes().size();


        LikeDto likeDto = integrationService.increaseMovieLikes(EXISTS_PARENT_MOVIE_5_ID, getCurrentUser());
        --likes;

        assertNotNull(likeDto);
        assertEquals((Integer)likes, likeDto.getLikes());
        assertFalse(likeDto.isLiked());

        movie = movieService.getByMovieIdWithSegments(EXISTS_PARENT_MOVIE_5_ID);

        assertNotNull(movie);
        assertNotNull(movie.getLikes());
        assertEquals(likes, movie.getLikes().size());
    }
}
