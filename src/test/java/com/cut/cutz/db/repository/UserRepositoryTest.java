package com.cut.cutz.db.repository;

import com.cut.cutz.constants.ErrorConstants;
import com.cut.cutz.db.base.dbunit.UserRepositoryBase;
import com.cut.cutz.db.entity.Genre;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.exception.UnexpectedDataException;
import com.cut.cutz.helper.DataHelper;
import com.cut.cutz.service.GenreService;
import com.cut.cutz.service.IntegrationService;
import com.cut.cutz.service.UserService;
import com.cut.cutz.web.dto.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;

import java.util.List;
import java.util.Set;

import static com.cut.cutz.constants.TestConstants.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * @author Pogorelov on 09.09.2015
 */

public class UserRepositoryTest extends UserRepositoryBase {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private GenreService genreService;
    @Autowired
    private IntegrationService integrationService;

    @Test
    public void loginNewUser() {
        User user = userRepository.findByEmail(NEW_USER_EMAIL);
        assertNull(user);

        final LoginDto loginDto = integrationService.userInformationOnLogin(NEW_USER_EMAIL, NEW_USER_PASSWORD, new MockHttpServletRequest());
        assertNotNull(loginDto);

        List<FollowerDto> suggestedPeople = loginDto.getSuggestedPeople();
        assertNotNull(suggestedPeople);
        assertEquals(4, suggestedPeople.size());
        assertEquals(1, (int)suggestedPeople.get(0).getUserId());
        assertEquals(2, (int)suggestedPeople.get(1).getUserId());
        assertEquals(3, (int)suggestedPeople.get(2).getUserId());

        final LoginUserDto loginUserDto = loginDto.getLoginUserDto();
        assertNotNull(loginUserDto);
        assertEquals(NEW_USER_EMAIL, loginUserDto.getEmail());

        final List<GenreDto> genreDtos = loginDto.getGenreDto();
        assertNotNull(genreDtos);
        assertEquals(6, genreDtos.size());
        for (GenreDto genreDto : genreDtos) {
            assertFalse(genreDto.getIsSelected());
        }

        user = userRepository.findByEmail(NEW_USER_EMAIL);
        assertNotNull(user);
    }

    @Test
    public void loginNewFBUser() {
        User user = userRepository.findByEmail(NEW_USER_EMAIL);
        assertNull(user);
        final String username = "Cut CutZ";
        final String photoUrl = "someUrl";

        final LoginDto loginDto = integrationService.userInformationOnFacebookLogin(username, photoUrl, NEW_USER_EMAIL, new MockHttpServletRequest());
        assertNotNull(loginDto);

        List<FollowerDto> suggestedPeople = loginDto.getSuggestedPeople();
        assertNotNull(suggestedPeople);
        assertEquals(4, suggestedPeople.size());
        assertEquals(1, (int)suggestedPeople.get(0).getUserId());
        assertEquals(2, (int)suggestedPeople.get(1).getUserId());
        assertEquals(3, (int)suggestedPeople.get(2).getUserId());

        final LoginUserDto loginUserDto = loginDto.getLoginUserDto();
        assertNotNull(loginUserDto);
        assertTrue(loginUserDto.getFbUser());
        assertEquals(NEW_USER_EMAIL, loginUserDto.getEmail());

        final List<GenreDto> genreDtos = loginDto.getGenreDto();
        assertNotNull(genreDtos);
        assertEquals(6, genreDtos.size());
        for (GenreDto genreDto : genreDtos) {
            assertFalse(genreDto.getIsSelected());
        }

        user = userRepository.findByEmail(NEW_USER_EMAIL);
        assertNotNull(user);
    }

    @Test
    public void loginAsFBUserThenChangePasswordLoginAsNormalUserAgain() throws Exception {
        User user = userRepository.findByEmail(NEW_USER_EMAIL);
        assertNull(user);
        final String username = "Cut CutZ";
        final String photoUrl = "someUrl";
        final String newPassword = "newPassword";

        LoginDto loginDto = integrationService.userInformationOnFacebookLogin(username, photoUrl, NEW_USER_EMAIL, new MockHttpServletRequest());
        assertNotNull(loginDto);

        LoginUserDto loginUserDto = loginDto.getLoginUserDto();
        assertNotNull(loginUserDto);
        assertTrue(loginUserDto.getFbUser());
        assertTrue(loginUserDto.getNewUser());
        assertEquals(loginUserDto.getUserName(), username);
        assertEquals(loginUserDto.getPhotoUrl(), photoUrl);

        UserDto userDto = userService.updateUserData(null, null, null, newPassword, null);
        assertNotNull(userDto);
        assertNull(userDto.getFbUser());
        assertNull(userDto.getNewUser());

        loginDto = integrationService.userInformationOnFacebookLogin(username, photoUrl, NEW_USER_EMAIL, new MockHttpServletRequest());
        assertNotNull(loginDto);

        loginUserDto = loginDto.getLoginUserDto();
        assertNotNull(loginUserDto);
        assertTrue(loginUserDto.getFbUser());
        assertFalse(loginUserDto.getNewUser());
    }

    @Test
    public void loginExistsUser() {
        final User user = userRepository.findByEmail(EXISTS_USER_EMAIL);
        assertNotNull(user);

        final LoginDto loginDto = integrationService.userInformationOnLogin(EXISTS_USER_EMAIL, EXISTS_USER_PASSWORD, new MockHttpServletRequest());
        assertNotNull(loginDto);

        final LoginUserDto loginUserDto = loginDto.getLoginUserDto();
        assertNotNull(loginUserDto);
        assertEquals(new Integer(2), loginUserDto.getFollowers());

        final Integer following = loginUserDto.getFollowing();
        assertNotNull(following);
        assertThat(following, is(3));

        final List<MovieDto> userMovies = loginUserDto.getUserMovies();
        assertNotNull(userMovies);
        assertEquals(1, userMovies.size());

        final List<MovieDto> myFeed = loginDto.getMyFeedMovies();
        assertNotNull(myFeed);
        assertEquals(4, myFeed.size());

        final List<MovieDto> newest = loginDto.getNewestMovies();
        assertNotNull(newest);
        assertEquals(5, newest.size());

        final List<MovieDto> popularMovies = loginDto.getPopularMovieDto();
        assertNotNull(popularMovies);
        assertEquals(5, popularMovies.size());

        final List<GenreDto> genres = loginDto.getGenreDto();
        assertNotNull(genres);
        assertEquals(6, genres.size());
        assertTrue(genres.get(0).getIsSelected());
        assertTrue(genres.get(5).getIsSelected());
    }

    @Test
    public void notAbleToLoginUser() {
        final User user = userRepository.findByEmail(EXISTS_USER_EMAIL);
        assertNotNull(user);

        try {
            integrationService.userInformationOnLogin(EXISTS_USER_EMAIL, "incorrect.password", new MockHttpServletRequest());
        } catch (UnexpectedDataException e) {
            assertEquals(ErrorConstants.BAD_CREDENTIALS, e.getMessage());
        }
    }

    @Test
    public void searchUsers() {
        final List<UserDto> userDtos = userService.searchUsersDtoByQuery("User");
        assertNotNull(userDtos);

        assertEquals(5, userDtos.size());
    }

    @Test
    public void searchUsersWithLowercase() {
        final List<UserDto> userDtos = userService.searchUsersDtoByQuery("user");
        assertNotNull(userDtos);

        assertEquals(5, userDtos.size());
    }

    @Test
    public void changeUserData() throws Exception {
        final String newEmail = "newUserName@email.com";
        final String newUserName = "Firstname Lastname";
        final String newPassword = "newPassword";
        final String oldPassword = EXISTS_USER_PASSWORD;

        User user = userRepository.findByEmail(EXISTS_USER_EMAIL);
        assertNotNull(user);
        assertNotEquals(newUserName, user.getName());
        assertEquals(DataHelper.encodedValue(oldPassword), user.getPassword());

        UserDto userDto = userService.updateUserData(newUserName, newEmail, null, newPassword, oldPassword);
        assertNotNull(userDto);

        user = userRepository.findByEmail(newEmail);
        assertNotNull(user);
        assertEquals(newUserName, user.getName());
        assertEquals(newUserName, userDto.getUserName());
        assertEquals(DataHelper.encodedValue(newPassword), user.getPassword());
    }

    @Test
    public void updateUserDataWithoutEmail() throws Exception {
        final String newUserName = "Firstname Lastname";
        final String newPassword = "newPassword";
        final String oldPassword = EXISTS_USER_PASSWORD;

        User user = userRepository.findByEmail(EXISTS_USER_EMAIL);
        assertNotNull(user);
        assertNotEquals(newUserName, user.getName());
        assertEquals(DataHelper.encodedValue(oldPassword), user.getPassword());

        UserDto userDto = userService.updateUserData(newUserName, null, null, newPassword, oldPassword);
        assertNotNull(userDto);

        user = userRepository.findByEmail(EXISTS_USER_EMAIL);
        assertNotNull(user);
        assertEquals(newUserName, user.getName());
        assertEquals(newUserName, userDto.getUserName());
        assertEquals(DataHelper.encodedValue(newPassword), user.getPassword());
    }

    @Test
    public void doNotChangeUserData() {
        final String newEmail = "test.user2@test.user2.com";
        final String newUserName = "NewUserName";
        final String newPassword = "newPassword";
        final String oldPassword = EXISTS_USER_PASSWORD;

        User user = userRepository.findByEmail(EXISTS_USER_EMAIL);
        assertNotNull(user);
        assertNotEquals(newUserName, user.getName());

        try {
            userService.updateUserData(newUserName, newEmail, null, newPassword, oldPassword);
        } catch (Exception e) {
            final String msg = "Email: " + newEmail + " already exists.";
            assertEquals(msg, e.getMessage());
        }
    }

    @Test
    public void follow5UsersByNewUser() throws Exception {
        final String currentUserEmail = "test.user6@test.user6.com";
        setMockCurrentUser(currentUserEmail);

        User currentUser = userService.findByEmailWithFollowing(currentUserEmail);
        assertNotNull(currentUser);
        assertEquals(0, currentUser.getFollowing().size());

        final List<User> users = userRepository.findAll();
        for (User follower : users) {
            if (!follower.getName().equals(currentUser.getName())) {
                userService.followUser(follower.getId());
            }
        }

        currentUser = userService.findByEmailWithFollowing(currentUserEmail);
        assertNotNull(currentUser);
        assertEquals(5, currentUser.getFollowing().size());
    }

    @Test
    public void unfollowUser() throws Exception {
        User currentUser = userService.findByEmailWithFollowing(EXISTS_USER_EMAIL);
        assertNotNull(currentUser);
        assertEquals(3, currentUser.getFollowing().size());

        final Integer user = 2;
        userService.followUser(user);

        currentUser = userService.findByEmailWithFollowing(EXISTS_USER_EMAIL);
        assertNotNull(currentUser);
        assertEquals(2, currentUser.getFollowing().size());
    }

    @Test
    public void shouldReceiveFollowersUser1() {
        Set<FollowerDto> followers = userService.getFollowers(1);
        assertNotNull(followers);
        assertThat(followers.size(), is(2));
    }

    @Test
    public void shouldReceiveFollowersUser2() {
        Set<FollowerDto> followers = userService.getFollowers(2);
        assertNotNull(followers);
        assertThat(followers.size(), is(2));
    }

    @Test
    public void shouldReceiveFollowersUser5() {
        Set<FollowerDto> followers = userService.getFollowers(5);
        assertNotNull(followers);
        assertThat(followers.size(), is(1));
    }

    @Test
    public void shouldReceiveFollowing1() {
        Set<FollowerDto> following = userService.getFollowing(1);
        assertNotNull(following);
        assertThat(following.size(), is(3));
    }

    @Test
    public void shouldReceiveFollowing2() {
        Set<FollowerDto> following = userService.getFollowing(2);
        assertNotNull(following);
        assertThat(following.size(), is(1));
    }


    @Test
    public void shouldReceiveFollowing5() {
        Set<FollowerDto> following = userService.getFollowing(5);
        assertNotNull(following);
        assertThat(following.size(), is(1));
    }

    @Test
    public void userShouldUnfollowUser() throws Exception {
        User someUser = userService.findByEmailWithFollowing(EXISTS_USER_EMAIL);
        assertNotNull(someUser);
        assertEquals(3, someUser.getFollowing().size());

        someUser = userService.findByEmailWithFollowers("test.user3@test.user3.com");
        assertNotNull(someUser);
        assertEquals(1, someUser.getFollowers().size());

        userService.followUser(someUser.getId());

        someUser = userService.findByEmailWithFollowing(EXISTS_USER_EMAIL);
        assertNotNull(someUser);
        assertEquals(2, someUser.getFollowing().size());

        someUser = userService.findByEmailWithFollowers("test.user3@test.user3.com");
        assertNotNull(someUser);
        assertEquals(0, someUser.getFollowers().size());
    }

    @Test
    public void userShouldFollowNewUser() throws Exception {
        User someUser = userService.findByEmailWithFollowing("test.user3@test.user3.com");
        assertNotNull(someUser);
        assertEquals(2, someUser.getFollowing().size());

        someUser = userService.findByEmailWithFollowers(EXISTS_USER_EMAIL);
        assertNotNull(someUser);
        assertEquals(2, someUser.getFollowers().size());

        userService.followUser(someUser.getId());

        someUser = userService.findByEmailWithFollowing("test.user3@test.user3.com");
        assertNotNull(someUser);
        assertEquals(2, someUser.getFollowing().size());

        someUser = userService.findByEmailWithFollowers(EXISTS_USER_EMAIL);
        assertNotNull(someUser);
        assertEquals(2, someUser.getFollowers().size());
    }

    @Test
    public void addFollowersAndFollowOther() throws Exception {
        final String currentUserEmail = "test.user6@test.user6.com";
        setMockCurrentUser(currentUserEmail);

        User currentUser = userService.findByEmailWithFollowersAndFollowing(currentUserEmail);
        assertNotNull(currentUser);
        assertEquals(0, currentUser.getFollowing().size());

        final List<User> users = userRepository.findAll();
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            if (!user.getName().equals(currentUser.getName())) {
                userService.followUser(user.getId());
            }
        }

        currentUser = userService.findByEmailWithFollowersAndFollowing(currentUserEmail);
        assertNotNull(currentUser);
        assertEquals(5, currentUser.getFollowing().size());

    }

    @Test
    public void recoverPasswordForUser() throws Exception {
        final User user = userRepository.findByEmail(EXISTS_USER_EMAIL);
        assertNotNull(user);

        userService.passwordReset(EXISTS_USER_EMAIL);

        emailTest();
    }

    @Test
    public void getAllNewGenresForUser() throws Exception {
        final List<GenreDto> genreDtoList = genreService.getAllGenreDtosByUserId(3);

        assertNotNull(genreDtoList);
        assertEquals(6, genreDtoList.size());
        for (GenreDto genreDto : genreDtoList) {
            assertFalse(genreDto.getIsSelected());
        }
    }

    @Test
    public void getAllGenresForUser() throws Exception {
        final List<GenreDto> genreDtoList = genreService.getAllGenreDtosByUserId(2);

        assertNotNull(genreDtoList);
        assertEquals(6, genreDtoList.size());
        for (int i = 0; i < genreDtoList.size(); i++) {
            GenreDto genreDto = genreDtoList.get(i);
            if (i < 4) {
                assertTrue(genreDto.getIsSelected());
            } else {
                assertFalse(genreDto.getIsSelected());
            }
        }
    }

    @Test
    public void getAllNewGenresWithoutUser() throws Exception {
        try {
            genreService.getAllGenreDtosByUserId(null);
        } catch (UnexpectedDataException e) {
            assertEquals(ErrorConstants.UNEXPECTED_USER, e.getMessage());
        }
    }

    @Test
    public void getSelfUser() throws Exception {
        final Integer currentUserId = userService.getCurrentUser().getId();

        final UserDto userDto = userService.getUserDtoById(currentUserId);

        assertNotNull(userDto);
        assertEquals("testUser", userDto.getUserName());
        assertEquals(1, userDto.getUserMovies().size());
        assertFalse(userDto.getHasRelations());
        assertEquals(new Integer(2), userDto.getFollowers());
    }


    @Test
    public void getSelfWithoutUserId() throws Exception {
        try {
            userService.getUserDtoById(null);
        } catch (UnexpectedDataException e) {
            assertEquals(ErrorConstants.UNEXPECTED_USER, e.getMessage());
        }
    }

    @Test
    public void getSuggestedUsers() throws Exception {
        List<FollowerDto> suggestedPeople = userService.getSuggestedPeople();

        assertThat(suggestedPeople, is(not(nullValue())));
        assertThat(suggestedPeople.size(), is(0));
    }

    @Test
    public void userHasNotRelationsWithTheCurrentUser() throws Exception {
        setMockCurrentUser("test.user6@test.user6.com");

        final Integer anotherUserId = 1;
        final UserDto userDto = userService.getUserDtoById(anotherUserId);

        assertNotNull(userDto);
        assertEquals("testUser", userDto.getUserName());
        assertEquals(1, userDto.getUserMovies().size());
        assertFalse(userDto.getHasRelations());
        assertEquals(new Integer(2), userDto.getFollowers());
    }

    @Test
    public void userHasRelationsWithTheCurrentUser() throws Exception {
        setMockCurrentUser("test.user3@test.user3.com");

        final Integer anotherUserId = 1;
        final UserDto userDto = userService.getUserDtoById(anotherUserId);

        assertNotNull(userDto);
        assertEquals("testUser", userDto.getUserName());
        assertEquals(1, userDto.getUserMovies().size());
        assertTrue(userDto.getHasRelations());
        assertEquals(new Integer(2), userDto.getFollowers());
    }

    @Test
    public void saveGenresAndFollowNewUsers() throws Exception {
        final String genresIDs = "1,2,3,4,5";
        final String followNewUsers = "3,4,5,6";

        final User mockUser = userRepository.findOne(2);
        userService.setCurrentUser(mockUser, new MockHttpServletRequest());

        integrationService.storeInitData(genresIDs, followNewUsers);

        final User user = userService.findByIdWithGenresAndFollowing(2);
        final List<Genre> genres = user.getGenres();
        final Set<User> following = user.getFollowing();

        assertNotNull(genres);
        assertEquals(5, genres.size());
        assertThat(genres, is(not(nullValue())));
        assertThat(genres.size(), is(5));
        assertThat(following, is(not(nullValue())));
        assertThat(following.size(), is(5));
    }

    @Test
    public void saveEmptyForUser() throws Exception {
        final String emptyGenres = "";
        final String empteFollowNewUsers = "";

        final User mockUser = userRepository.findOne(2);
        userService.setCurrentUser(mockUser, new MockHttpServletRequest());

        integrationService.storeInitData(emptyGenres, empteFollowNewUsers);

        final User user = userService.findByIdWithGenresAndFollowing(2);
        final List<Genre> genres = user.getGenres();
        final Set<User> following = user.getFollowing();

        assertThat(genres, is(not(nullValue())));
        assertThat(genres.size(), is(0));
        assertThat(following, is(not(nullValue())));
        assertThat(following.size(), is(1));
    }
}

