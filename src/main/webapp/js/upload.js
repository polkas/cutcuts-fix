/**
 * Created by Pogorelov on 12/20/2015.
 */
$(document).on('change', '.btn-file :file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

var segmentObj = {
    iterator: 0,
    addSegment: function () {
        var index = this.iterator + 1;

        var label = '<label class="segment-label nav-bcol-xs-2 col-sm-2">Segment ' + index + ':</label>';

        var start = '<div class="col-xs-3 col-sm-2">' +
            '<input type="text" class="form-control" value="" placeholder="Start  (in sec. \'0.0\')" id="start' + index + '">' +
            '</div>';

        var end = '<div class="col-xs-4 col-sm-2">' +
            '<input type="text" class="form-control" value="" placeholder="End  (in sec. \'0.0\')" id="end' + index + '">' +
            '</div>';

        var position = '<div class="col-xs-5 col-sm-2">' +
            '<input type="text" class="form-control" value="" placeholder="Nickname" id="nickname' + index + '">' +
            '</div>';

        var picture = '<div class="col-xs-6 col-sm-4">' +
            '<div class="input-group">' +
            '<span class="input-group-btn">' +
            '<span class="btn btn-default btn-file">Upload pic<input type="file" multiple id="actorPic' + index + '"> </span>' +
            '</span>' +
            '<input type="text" class="form-control" readonly>' +
            '</div>' +
            '</div>';

        var segment = label + start + end + position + picture;

        return '<div class="form-group">' + segment + '</div><br/><br/>';
    }
};

$(document).ready(function () {

    $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

        console.log('file set on.');

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;


        console.log(input);
        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });
    var segmentsArea = $('#segments');

    $('#Logout').click(function () {
        $.ajax({
            type: "POST",
            url: "/api/rest/logout"
        });
    });

    $('#addSegmentBtn').click(function () {
        segmentsArea.append(segmentObj.addSegment());
        ++segmentObj.iterator;
    });

    $('#uploadBtn').click(function () {
        var movieName = $('#movieName').val();
        var file = $('#movieFile')[0].files[0];
        var posterFile = $('#posterFile')[0].files[0];
        var genreId = $('#genreId').val();
        var duration = $('#duration').val();
        var finalDuration = duration * 1000;
        var description = $('#description').val();


        var formData = new FormData();
        formData.append('name', movieName);
        formData.append('file', file);
        formData.append('posterFile', posterFile);
        formData.append('genreId', genreId);
        formData.append('duration', finalDuration);
        formData.append('description', description);

        for (var i = 0; i < segmentObj.iterator; i++) {
            var index = i + 1;
            var start = $('#start' + index).val();
            var finalStart = start * 1000;
            var end = $('#end' + index).val();
            var finalEnd = end * 1000;
            var actorNickname = $('#nickname' + index).val();
            var actorPicture = $('#actorPic' + index)[0].files[0];

            formData.append('segments[' + i + '].start', finalStart);
            formData.append('segments[' + i + '].end', finalEnd);
            formData.append('segments[' + i + '].actorUpload.nickname', actorNickname);
            formData.append('segments[' + i + '].actorUpload.picture', actorPicture);
        }

        console.log("form data " + formData);
        $.ajax({
            url: '/api/rest/movie/upload',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
                $('.loader').show();
            },
            complete: function () {
                $('.loader').hide();
            },
            success: function (data) {
                var done = '<div style="text-align: center;"> <h1>Done :) </h1> </div>';
                $('#dialogContent').css('background-color', '#2ECC71');
                $('#dialogContent').html(done);
                $('#dialogMessage').modal('show');
            },
            error: function (err) {
                console.log(err);
                var error = '<div style="text-align: center;"> <h4>ERROR: ' + err.responseJSON.error_message + '</h4> </div>';
                $('#dialogContent').css('background-color', '#C0392B');
                $('#dialogContent').html(error);
                $('#dialogMessage').modal('show');
            }
        });
    });
});