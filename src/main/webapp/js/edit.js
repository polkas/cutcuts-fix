/**
 * Created by Pogorelov on 12/20/2015.
 */
var videoObj = {
    addMain: function (name, duration, description) {
        var name = '<div class="col-xs-1 col-sm-3"><div class="form-group"><input type="text" class="form-control" value="' + name + '" placeholder="Movie name" id="movieName"></div></div>';
        var duration = '<div class="col-xs-1 col-sm-3"><div class="form-group"><input type="text" class="form-control" value="' + duration + '" placeholder="Duration(in seconds, \'2.5\')" id="movieDuration"></div></div>';
        var genre = '<div class="col-xs-1 col-sm-3"><div class="form-group">' +
            '<select class="form-control select select-primary" id="genreId">' +
            '<option value="0">Choose genre</option>' +
            '<option value="1">Movies</option>' +
            '<option value="2">TV Commercials</option>' +
            '<option value="3">TV Series</option>' +
            '<option value="4">Cartoons</option>' +
            '<option value="5">Politics</option>' +
            '<option value="6">Sports</option>' +
            '</select>' +
            '</div></div>';

        var description = '<div class="col-xs-1 col-sm-3"><div class="form-group"><textarea type="text" rows="5" class="form-control" placeholder="Description" id="movieDescription">' + description + '</textarea></div></div>';

        return name + duration + genre + description;
    },
    chengeGenre: function (genre) {
        var option = $('#genreId option').filter(function () {
            return $(this).html() == genre;
        }).val();
        $('#genreId').val(option);
    }

};

var segmentObj = {
    iterator: 0,
    ids_array: [],
    clean: function() {
        this.iterator = 0;
        this.ids_array = [];
    },
    addSegment: function (index, start, end, position) {
        ++this.iterator;

        var label = '<label class="segment-label nav-bcol-xs-2 col-sm-2" style="width: 10%;  text-align: left; font-size: 17px;">Segment ' + index + ':</label>';

        var start = '<div class="col-xs-2 col-sm-1">' +
            '<input type="text" class="form-control" value="' + start + '" placeholder="Start \'0.0\'" id="start' + index + '">' +
            '</div>';

        var end = '<div class="col-xs-2 col-sm-1" style="margin-left: 0.5%">' +
            '<input type="text" class="form-control" value="' + end + '" placeholder="End \'0.0\'" id="end' + index + '">' +
            '</div>';

        var position = '<div class="col-xs-1 col-sm-3">' +
            '<input type="text" class="form-control" value="' + position + '" placeholder="Nickname" id="nickname' + index + '">' +
            '</div>';

        var segment = label + start + end + position;

        return '<div class="form-group">' + segment + '</div><br/><br/>';
    }
};

$(document).ready(function () {
    var movieId;

    var mainVideo = $('#videoHeader');
    var segmentsArea = $('#segments');
    var updateBtn = $('#updateBtn');
    var deleteBtn = $('#deleteBtn');

    $('#searchBtn').click(function () {
        movieId = undefined;
        segmentObj.clean();

        mainVideo.text('');
        segmentsArea.text('');

        var videoName = $('#video_name').val();
        $.ajax({
            url: '/api/rest/movie/edit',
            data: "query=" + videoName,
            type: 'GET',
            beforeSend: function(){
                $('.loader').show();
            },
            complete: function(){
                $('.loader').hide();
            },
            success: function (data) {
                console.log(data);

                var name = data.name, duration = data.duration / 1000, genre = data.genre, description = data.description;

                console.log( data.duration);
                mainVideo.append(videoObj.addMain(name, duration, description));
                videoObj.chengeGenre(genre);

                var segments = data.segments;
                for (var i = 0; i < segments.length; i++) {
                    segmentObj.ids_array[segmentObj.iterator] = segments[i].id;
                    segmentsArea.append(segmentObj.addSegment(i + 1, segments[i].start / 1000, segments[i].end / 1000, segments[i].actor.nickname));
                }

                updateBtn.css("display", "block");
                deleteBtn.css("display", "block");
                movieId = data.id;
            },
            error: function () {
                segmentObj.clean();
                movieId = undefined;
                console.log('error');
                var error = '<div style="text-align: center;"> <h4>Nothing found  :(</h4> </div>';
                $('#dialogContent').css('background-color', '#2ECC71');
                $('#dialogContent').html(error);
                $('#dialogMessage').modal('show');
                console.log('Nothing found by name: ' + videoName);
            }
        });
    });

    $('#Logout').click(function () {
        $.ajax({
            type: "POST",
            url: "/api/rest/logout"
        });
    });

    updateBtn.click(function () {
        if (confirm("Are you sure?") === true) {
            var movieName = $('#movieName').val();
            var genreId = $('#genreId').val();
            var duration = $('#movieDuration').val() * 1000;
            var description = $('#movieDescription').val();

            console.log(movieName + " " + genreId + " " + duration + " " + description);

            var formData = new FormData();
            formData.append('name', movieName);
            formData.append('genreId', genreId);
            formData.append('duration', duration);
            formData.append('description', description);

            for (var i = 0; i < segmentObj.iterator; i++) {
                var index = i + 1;
                console.log(i);
                var segmentId = segmentObj.ids_array[i];
                var start = $('#start' + index).val() * 1000;
                var end = $('#end' + index).val() * 1000;
                var actorNickname = $('#nickname' + index).val();

                console.log(segmentId + " " + start + " " + end + " " + actorNickname);
                formData.append('segments[' + i + '].segmentId', segmentId);
                formData.append('segments[' + i + '].start', start);
                formData.append('segments[' + i + '].end', end);
                formData.append('segments[' + i + '].actorUpload.nickname', actorNickname);
            }

            console.log("form data:");
            console.log(formData);

            $.ajax({
                url: '/api/rest/movie/' + movieId + '/update',
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend: function(){
                    $('.loader').show();
                },
                complete: function(){
                    $('.loader').hide();
                },
                success: function (data) {
                    console.log(data);

                    mainVideo.text('');
                    segmentsArea.text('');
                    updateBtn.css("display", "none");
                    deleteBtn.css("display", "none");
                    movieId = undefined;
                    segmentObj.clean();
                },
                error: function () {
                    console.log('error');

                }
            });


            mainVideo.text('');
            segmentsArea.text('');
            updateBtn.css("display", "none");
            deleteBtn.css("display", "none");
        }
    });

    deleteBtn.click(function () {
        if (confirm("Are you sure?") === true) {

            $.ajax({
                url: '/api/rest/movie/' + movieId,
                type: 'DELETE',
                beforeSend: function(){
                    $('.loader').show();
                },
                complete: function(){
                    $('.loader').hide();
                },
                success: function (data) {
                    console.log(data);

                    mainVideo.text('');
                    segmentsArea.text('');
                    updateBtn.css("display", "none");
                    deleteBtn.css("display", "none");
                    movieId = undefined;
                    segmentObj.clean();
                },
                error: function () {
                    console.log('error');

                }
            });
        }
    });


    //$('#123').click(function () {
    //    var movieName = $('#movieName').val();
    //    var file = $('#movieFile')[0].files[0];
    //    var posterFile = $('#posterFile')[0].files[0];
    //    var genreId = $('#genreId').val();
    //    var duration = $('#duration').val();
    //    var finalDuration = duration * 1000;
    //    var description = $('#description').val();
    //
    //
    //    var formData = new FormData();
    //    formData.append('name', movieName);
    //    formData.append('file', file);
    //    formData.append('posterFile', posterFile);
    //    formData.append('genreId', genreId);
    //    formData.append('duration', finalDuration);
    //    formData.append('description', description);
    //
    //    for (var i = 0; i < segmentObj.iterator; i++) {
    //        var index = i + 1;
    //        var start = $('#start' + index).val();
    //        var finalStart = start * 1000;
    //        var end = $('#end' + index).val();
    //        var finalEnd = end * 1000;
    //        var actorNickname = $('#nickname' + index).val();
    //        var actorPicture = $('#actorPic' + index)[0].files[0];
    //
    //        formData.append('segments[' + i + '].start', finalStart);
    //        formData.append('segments[' + i + '].end', finalEnd);
    //        formData.append('segments[' + i + '].actorUpload.nickname', actorNickname);
    //        formData.append('segments[' + i + '].actorUpload.picture', actorPicture);
    //    }
    //
    //    console.log("form data " + formData);
    //    $.ajax({
    //        url: '/api/rest/movie/upload',
    //        data: formData,
    //        processData: false,
    //        contentType: false,
    //        type: 'POST',
    //        beforeSend: function () {
    //            $('.loader').show();
    //        },
    //        complete: function () {
    //            $('.loader').hide();
    //        },
    //        success: function (data) {
    //            var done = '<div style="text-align: center;"> <h1>Done :) </h1> </div>';
    //            $('#dialogContent').css('background-color', '#2ECC71');
    //            $('#dialogContent').html(done);
    //            $('#dialogMessage').modal('show');
    //        },
    //        error: function (err) {
    //            var error = '<div style="text-align: center;"> <h4>ERROR:' + err.responseText + '</h4> </div>';
    //            $('#dialogContent').css('background-color', '#C0392B');
    //            $('#dialogContent').html(error);
    //            $('#dialogMessage').modal('show');
    //        }
    //    });
    //});
});