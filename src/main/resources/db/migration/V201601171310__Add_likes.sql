ALTER TABLE `movie`
CHANGE COLUMN `duration` `duration` INT(5) NOT NULL ,
ADD COLUMN `likes` INT(10) NOT NULL DEFAULT '0' AFTER `views`;
