DROP TABLE IF EXISTS `record`;

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `comment_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `author_id` INT(11) NOT NULL,
  `movie_id` BIGINT(20) NOT NULL,
  `text` TEXT NOT NULL,
  PRIMARY KEY (`comment_id`),
  INDEX `fk_comment_user_id_idx` (`author_id` ASC),
  INDEX `fk_comment_movie_id_idx` (`movie_id` ASC),
  CONSTRAINT `fk_comment_user_id`
    FOREIGN KEY (`author_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_comment_movie_id`
    FOREIGN KEY (`movie_id`)
    REFERENCES `movie` (`movie_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

