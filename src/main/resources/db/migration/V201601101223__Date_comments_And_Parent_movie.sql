ALTER TABLE `comment`
ADD COLUMN `date` TIMESTAMP NOT NULL AFTER `text`;


ALTER TABLE `movie`
DROP FOREIGN KEY `fk_movie_to_parent_id`;

ALTER TABLE `movie`
ADD CONSTRAINT `fk_movie_to_parent_id`
  FOREIGN KEY (`parent`)
  REFERENCES `movie` (`movie_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
