ALTER TABLE `segment`
ADD COLUMN `actor_position` VARCHAR(255) NOT NULL AFTER `end_time`,
ADD COLUMN `actor_picture_url` VARCHAR(255) NOT NULL AFTER `actor_position`;

INSERT INTO `segment` (`segment_id`, `movie_id`, `start_time`, `end_time`, `actor_position`, `actor_picture_url`) VALUES ('1', '1', '0', '15000', 'actor1', 'url1');
INSERT INTO `segment` (`segment_id`, `movie_id`, `start_time`, `end_time`, `actor_position`, `actor_picture_url`) VALUES ('3', '2', '0', '5000', 'actor2', 'url2');
INSERT INTO `segment` (`segment_id`, `movie_id`, `start_time`, `end_time`, `actor_position`, `actor_picture_url`) VALUES ('4', '2', '10000', '15000', 'actor3', 'url3');