CREATE TABLE `user` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`user_id`));


CREATE TABLE `genre` (
  `genre_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`genre_id`));


CREATE TABLE `user_genre` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `user_id` INT(11) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_idx` (`user_id` ASC),
  INDEX `fk_genre_idx` (`genre_id` ASC),
  CONSTRAINT `fk_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_genre`
    FOREIGN KEY (`genre_id`)
    REFERENCES `genre` (`genre_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);



INSERT INTO `genre` (`genre_id`, `name`, `url`) VALUES ('1', 'Movies', 'http://cutcutz-ourapplication.rhcloud.com/storage/genres/movies.jpg');
INSERT INTO `genre` (`genre_id`, `name`, `url`) VALUES ('2', 'TV Commercials', 'http://cutcutz-ourapplication.rhcloud.com/storage/genres/tv_commercials.jpg');
INSERT INTO `genre` (`genre_id`, `name`, `url`) VALUES ('3', 'TV Series', 'http://cutcutz-ourapplication.rhcloud.com/storage/genres/tv_series.jpg');
INSERT INTO `genre` (`genre_id`, `name`, `url`) VALUES ('4', 'Cartoons', 'http://cutcutz-ourapplication.rhcloud.com/storage/genres/cartoons.jpg');
INSERT INTO `genre` (`genre_id`, `name`, `url`) VALUES ('5', 'Politics', 'http://cutcutz-ourapplication.rhcloud.com/storage/genres/politics.jpg');
INSERT INTO `genre` (`genre_id`, `name`, `url`) VALUES ('6', 'Sports', 'http://cutcutz-ourapplication.rhcloud.com/storage/genres/sports.jpg');
