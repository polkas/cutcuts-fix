DROP TABLE IF EXISTS `segment`;

CREATE TABLE `segment` (
  `segment_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `movie_id` BIGINT(20) NOT NULL,
  `start_time` BIGINT(20) NOT NULL,
  `end_time` BIGINT(20) NOT NULL,
  PRIMARY KEY (`segment_id`),
  INDEX `fk_segment_to_movie_idx` (`movie_id` ASC),
  CONSTRAINT `fk_segment_to_movie`
    FOREIGN KEY (`movie_id`)
    REFERENCES `movie` (`movie_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);