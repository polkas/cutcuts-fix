ALTER TABLE `user`
ADD COLUMN `photo_url` VARCHAR(255) NOT NULL AFTER `password`,
ADD COLUMN `theme` INT(1) NOT NULL DEFAULT 0 AFTER `photo_url`;


CREATE TABLE `user_followers` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `user_follower_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_to_user_idx` (`user_id` ASC),
  INDEX `fk_user_follower_to_user_idx` (`user_follower_id` ASC),
  CONSTRAINT `fk_user_to_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_follower_to_user`
    FOREIGN KEY (`user_follower_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
