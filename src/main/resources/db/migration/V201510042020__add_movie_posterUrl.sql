ALTER TABLE `movie`
CHANGE COLUMN `url` `movie_url` VARCHAR(255) NOT NULL ,
ADD COLUMN `poster_url` VARCHAR(255) NOT NULL AFTER `movie_url`;

UPDATE `movie` SET `poster_url`='http://cutcutz-ourapplication.rhcloud.com/storage/posters/BlackMass.jpg' WHERE `movie_id`='1';
UPDATE `movie` SET `poster_url`='http://cutcutz-ourapplication.rhcloud.com/storage/posters/ThePursuitOfBetter.jpg' WHERE `movie_id`='2';
