CREATE TABLE `movie` (
  `movie_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `genre_id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `author_id` INT(11) NOT NULL,
  `views` INT(11) NULL DEFAULT 0,
  `date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` VARCHAR(255) NULL,
  `url` VARCHAR(255) NOT NULL,
  `duration` INT NOT NULL,
  `parent` BIGINT(20) NULL,
  PRIMARY KEY (`movie_id`),
  INDEX `fk_movie_to_genre_id_idx` (`genre_id` ASC),
  INDEX `fk_movie_to_author_id_idx` (`author_id` ASC),
  INDEX `fk_movie_to_parent_id_idx` (`parent` ASC),
  CONSTRAINT `fk_movie_to_genre_id`
    FOREIGN KEY (`genre_id`)
    REFERENCES `genre` (`genre_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_movie_to_author_id`
    FOREIGN KEY (`author_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_movie_to_parent_id`
    FOREIGN KEY (`parent`)
    REFERENCES `cutcutz`.`movie` (`movie_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE `segment` (
  `segment_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `movie_id` BIGINT(20) NOT NULL,
  `start_time` TIME NOT NULL,
  `end_time` TIME NOT NULL,
  PRIMARY KEY (`segment_id`),
  INDEX `fk_segment_to_movie_idx` (`movie_id` ASC),
  CONSTRAINT `fk_segment_to_movie`
    FOREIGN KEY (`movie_id`)
    REFERENCES `movie` (`movie_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


INSERT INTO `user` (`user_id`, `name`, `email`, `password`) VALUES ('1', 'Admin', 'cutcutzapp@gmail.com', 'c52cba5033c193be99c186409354688f2d585e32');


INSERT INTO `user_genre` (`id`, `user_id`, `genre_id`) VALUES ('1', '1', '1');
INSERT INTO `user_genre` (`id`, `user_id`, `genre_id`) VALUES ('2', '1', '2');
INSERT INTO `user_genre` (`id`, `user_id`, `genre_id`) VALUES ('3', '1', '3');
INSERT INTO `user_genre` (`id`, `user_id`, `genre_id`) VALUES ('4', '1', '4');
INSERT INTO `user_genre` (`id`, `user_id`, `genre_id`) VALUES ('5', '1', '5');
INSERT INTO `user_genre` (`id`, `user_id`, `genre_id`) VALUES ('6', '1', '6');


INSERT INTO `movie` (`movie_id`, `genre_id`, `name`, `author_id`, `views`, `url`, `duration`) VALUES ('1', '1', 'Black Mass', '1', '0', 'http://cutcutz-ourapplication.rhcloud.com/storage/videos/BlackMass.mp4', '15000');
INSERT INTO `movie` (`movie_id`, `genre_id`, `name`, `author_id`, `views`, `url`, `duration`) VALUES ('2', '6', 'The Pursuit Of Better', '1', '0', 'http://cutcutz-ourapplication.rhcloud.com/storage/videos/ThePursuitOfBetter.mp4', '15000');


INSERT INTO `segment` (`segment_id`, `movie_id`, `start_time`, `end_time`) VALUES ('1', '1', '00:00:02', '00:00:07');
INSERT INTO `segment` (`segment_id`, `movie_id`, `start_time`, `end_time`) VALUES ('2', '1', '00:00:09', '00:00:13');
INSERT INTO `segment` (`segment_id`, `movie_id`, `start_time`, `end_time`) VALUES ('3', '2', '00:00:00', '00:00:05');
INSERT INTO `segment` (`segment_id`, `movie_id`, `start_time`, `end_time`) VALUES ('4', '2', '00:00:10', '00:00:15');
