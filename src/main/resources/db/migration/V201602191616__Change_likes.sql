CREATE TABLE `like_table` (
  `like_id` BIGINT NOT NULL AUTO_INCREMENT,
  `movie_id` BIGINT(20) NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`like_id`),
  INDEX `fk_movie_id_idx` (`movie_id` ASC),
  INDEX `fk_user_id_idx` (`user_id` ASC),
  CONSTRAINT `fk_movie_id`
    FOREIGN KEY (`movie_id`)
    REFERENCES `movie` (`movie_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


ALTER TABLE `movie`
DROP COLUMN `likes`;
