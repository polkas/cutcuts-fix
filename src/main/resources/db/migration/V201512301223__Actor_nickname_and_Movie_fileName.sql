ALTER TABLE `segment`
CHANGE COLUMN `actor_position` `actor_nickname` VARCHAR(255) NOT NULL ;

ALTER TABLE `movie`
ADD COLUMN `file_name` VARCHAR(255) NOT NULL AFTER `name`;
