package com.cut.cutz.notification;

import com.cut.cutz.constants.Constants;
import com.cut.cutz.db.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;


/**
 * @author Pogorelov on 11.09.2015
 */
@Service
@PropertySource("classpath:properties/email.properties")
public class EmailSenderService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${mail.from}")
    private String from;

    @Async
    public void sendToUser(final User user, final String password) {
        String message = emailBuilder(user.getName(), password);
        final String subject = Constants.EMAIL_USER_SUBJECT;
        sendGmail(user.getEmail(), subject, message);
    }

    private void sendGmail(final String recipient, final String subject, final String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(from);
        mailMessage.setTo(recipient);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);
        javaMailSender.send(mailMessage);
    }


    private String emailBuilder(final String name, final String password) {
        StringBuilder sb = new StringBuilder(String.format(Constants.EMAIL_HEADER_DEAR_TEMPLATE,
                name == null ? Constants.EMAIL_DEFAULT_USER_NAME : name))
                .append(String.format(Constants.EMAIL_USER_PASSWORD_RECOVERY, password));

        return sb.toString();
    }
}

