package com.cut.cutz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author Pogorelov on 08.09.2015
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableAsync
public class CutCutZApplication extends SpringBootServletInitializer {

    //1 year = 365 d * 24 h = 8760 hours
    private final int ONE_YEAR = 8760 * 60 * 60;

    private static Class<CutCutZApplication> appClass = CutCutZApplication.class;

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(CutCutZApplication.class);

        springApplication.setShowBanner(false);

        springApplication.run(args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(appClass);
    }

    @Bean
    public ServletContextInitializer servletContextInitializer() {
        return servletContext -> {
            servletContext.getSessionCookieConfig().setName("JSESSIONID");
            servletContext.getSessionCookieConfig().setMaxAge(ONE_YEAR);
        };

    }
}