package com.cut.cutz.manager;

import com.cut.cutz.constants.Constants;
import com.cut.cutz.service.PropertiesService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * @author Pogorelov on 8/31/2016
 */

@Service
public class StorageManager {

    @Inject
    private PropertiesService propertiesService;

    public String getMovieStorage() {
        return propertiesService.getStorageFolder() + Constants.MOVIES_FOLDER_PATH;
    }

    public String getSoundTempStorage() {
        return propertiesService.getStorageFolder() + Constants.SOUND_TEMP_FOLDER_PATH;
    }

    public String getFullMovieUrl(final String fileName) {
        return propertiesService.getStorageServerURL() + Constants.STORAGE_FOLDER_PATH +
                Constants.MOVIES_FOLDER_PATH + fileName;
    }

    public String getPhotosStorage() {
        return propertiesService.getStorageFolder() + Constants.PHOTOS_FOLDER_PATH;
    }

    public String getFullPhotoUrl(final String fileName) {
        return propertiesService.getStorageServerURL() + Constants.STORAGE_FOLDER_PATH
                + Constants.PHOTOS_FOLDER_PATH + fileName;
    }

    public String getLogoPath() {
        return propertiesService.getLogoPath();
    }

}
