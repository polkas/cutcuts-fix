package com.cut.cutz.manager;

import com.coremedia.iso.boxes.Container;
import com.cut.cutz.helper.DataHelper;
import com.cut.cutz.holder.SegmentsRecordHolder;
import com.cut.cutz.logging.InjectLogger;
import com.cut.cutz.utils.SilenceTrackImpl;
import com.googlecode.mp4parser.MemoryDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pogorelov on 09.10.2015
 */
@Service
public class MovieManager {

    @InjectLogger
    private Logger log;

    @Inject
    private StorageManager storageManager;

    public void cutTheSoundFromMovie(String folderPath, String fileName, byte[] data) throws Exception {
        log.info("\nStart cut the sound for movie.");
        Movie video = MovieCreator.build(new MemoryDataSourceImpl(data));

        List<Track> videoTracks = video.getTracks();
        video.setTracks(new ArrayList<>());

        for (Track tr : videoTracks) {
            if (tr.getHandler().equals("vide")) {
                video.addTrack(tr);
            }
        }

        FileOutputStream fos = null;
        File timeMovieFile = null;
        try {
            Container out = new DefaultMp4Builder().build(video);
            final String newVideoFilePath = folderPath + fileName;
            final String tempFilePath = folderPath + "TEMP" + "_temp.mp4";
            timeMovieFile = new File(tempFilePath);

            fos = new FileOutputStream(timeMovieFile);
            out.writeContainer(fos.getChannel());

            log.info("\nFile without sound has been saved!");

            runFFmpeg(tempFilePath, newVideoFilePath);

        } finally {
            if(fos != null) fos.close();

            FileUtils.forceDelete(timeMovieFile);

            log.info("\n the sound method. Finally \"block\".");
        }
    }

    private void runFFmpeg(final String tempFileNath, final String videoFilePath) throws Exception {
        log.info("\nStart adding watermark logo in the video!");

        String[] command = {"ffmpeg", "-i", tempFileNath, "-i", storageManager.getLogoPath(),
        "-filter_complex", "[1:v]scale=140:140[wm];[0:v][wm]overlay=(main_w-overlay_w-25):(main_h-overlay_h-25)",
                videoFilePath};

        printFFmpegCommand(command);

        Process processDuration = new ProcessBuilder(command).redirectErrorStream(true).start();
        StringBuilder strBuild = new StringBuilder();
        try (BufferedReader processOutputReader = new BufferedReader(new InputStreamReader(processDuration.getInputStream(), Charset.defaultCharset()));) {
            String line;
            while ((line = processOutputReader.readLine()) != null) {
                strBuild.append(line).append(System.lineSeparator());
            }
            processDuration.waitFor();
        }
        String outputJson = strBuild.toString().trim();

        log.info(outputJson);


        log.info("\nWatermark has been added!\n");
    }

    private void printFFmpegCommand(String[] commands) {
        StringBuilder sb = new StringBuilder();
        for (String command : commands) {
            sb.append(command).append(" ");
        }
        log.info("\nFFmpeg command: " + sb.toString());
    }

    public String mergeWithFFmpeg(final String videoName, final String tempSoundNameWithPath) throws Exception {
        final String webAppVideoFolder = storageManager.getMovieStorage();

        final String newFileName = DataHelper.generateRandomUUID() + ".mp4";

        String[] command = {"ffmpeg", "-i", webAppVideoFolder + videoName, "-i", tempSoundNameWithPath,
                "-c:v", "copy", "-c:a", "copy",   webAppVideoFolder + newFileName};

        printFFmpegCommand(command);

        Process processDuration = new ProcessBuilder(command).redirectErrorStream(true).start();
        StringBuilder strBuild = new StringBuilder();
        try (BufferedReader processOutputReader = new BufferedReader(new InputStreamReader(processDuration.getInputStream(), Charset.defaultCharset()));) {
            String line;
            while ((line = processOutputReader.readLine()) != null) {
                strBuild.append(line).append(System.lineSeparator());
            }
            processDuration.waitFor();
        }

        String outputJson = strBuild.toString().trim();

        log.info(outputJson);


        log.info("\nMovie has been merged!\n");

        return newFileName;
    }

    public String mergeVideoWithSound(final String videoName,
                                      List<SegmentsRecordHolder> segmentsRecordHolders) throws Exception {

        final String webAppVideoFolder = storageManager.getMovieStorage();

        Movie video = MovieCreator.build(webAppVideoFolder + videoName);
        video.setTracks(new ArrayList<>());

        File videoNewFile;
        FileOutputStream fosVideoFile = null;

        try {
            List<Track> trackList = new ArrayList<>();

            long lastEndTime = 0;

            for (SegmentsRecordHolder segmentsRecordHolder : segmentsRecordHolders) {
                final CroppedTrack track = segmentsRecordHolder.getTrack();

                long endTime = segmentsRecordHolder.getEndTime();
                final long startTime = segmentsRecordHolder.getStartTime();

                log.info("Start parse video.");

                Movie soundMovie = new Movie();
                soundMovie.addTrack(track);
                //MovieCreator.build(new MemoryDataSourceImpl(soundInBytes));

                log.info("finish parse video. \n Start merge video.");

                for (int i = 0; i < soundMovie.getTracks().size(); i++) {
                    Track tr = soundMovie.getTracks().get(i);
                    if (tr.getHandler().equals("soun")) {
                        String soundType = tr.getSampleDescriptionBox().getSampleEntry().getType();
                        log.info("Sound TYPE: " + soundType);
                        if(!soundType.equals("mp4a")) {
                            final String msg = "The sound type: " + soundType + " does not support.";
                            log.error(msg);
                            throw new IllegalArgumentException(msg);
                        }

                        long[] sampleDurations = tr.getSampleDurations();
                        double timescale = tr.getTrackMetaData().getTimescale();

                        endTime = calculateEndTimeFromRecord(startTime, endTime, sampleDurations, timescale);

                        long silenceValue = startTime - lastEndTime;
                        log.info("Segment start time: " + startTime);
                        log.info("Segment end time: " + endTime);
                        log.info("LastEndTime: " + lastEndTime);
                        log.info("timescale: " + timescale);

                        double numFrames = timescale * silenceValue / 1000L / 1024L;
                        if (numFrames >= 1) {
                            log.info("start adding silence. " + silenceValue);
                            Track silenceTrack = new SilenceTrackImpl(tr, silenceValue);
                            log.info("finish adding silence.");
                            trackList.add(silenceTrack);
                        }

                        trackList.add(tr);

                        lastEndTime = endTime;
                    }
                }
            }

            final String newFileNameWithpath = storageManager.getSoundTempStorage() + DataHelper.generateRandomUUID() + ".m4a";
            videoNewFile = new File(newFileNameWithpath);

            final Track[] tracks = trackList.toArray(new Track[trackList.size()]);
            video.addTrack(new AppendTrack(tracks));


            Container mp4file = new DefaultMp4Builder().build(video);
            fosVideoFile = new FileOutputStream(videoNewFile);
            mp4file.writeContainer(fosVideoFile.getChannel());

            return mergeWithFFmpeg(videoName, newFileNameWithpath);
        } catch (ArrayIndexOutOfBoundsException e) {
            String msg = "The duration of video and music are different.";
            throw new Exception(msg);
        } finally {
            if (fosVideoFile != null) fosVideoFile.close();
        }
    }

    private long calculateEndTimeFromRecord(long startTime, long endTime, long[] sampleDurations, double timescale) {
        double resultDuration = 0;
        for (int j = 0; j < sampleDurations.length; j++) {
            resultDuration += (double) sampleDurations[j] / timescale;
        }

        final long segmentDurationInSec = Math.round((endTime - startTime) / 1000);
        final long recordDurationInSec = Math.round(resultDuration);

        //If record is less than segment size. Return new end time;
        if (recordDurationInSec != segmentDurationInSec) {
            return recordDurationInSec * 1000;
        }

        return endTime;
    }
}
