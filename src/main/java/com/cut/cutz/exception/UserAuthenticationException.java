package com.cut.cutz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Pogorelov on 10.09.2015
 */
@ResponseStatus(value = HttpStatus.CONFLICT)
public class UserAuthenticationException extends RuntimeException {

    private String message;

    public UserAuthenticationException(String msg) {
        this.message = msg;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
