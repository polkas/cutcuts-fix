package com.cut.cutz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Pogorelov on 1/2/2016
 */
@ResponseStatus(value = HttpStatus.CONFLICT)
public class UnexpectedDataException extends RuntimeException {

    private String message;

    public UnexpectedDataException(String msg) {
        this.message = msg;
    }

    @Override
    public String getMessage() {
        return message;
    }


}
