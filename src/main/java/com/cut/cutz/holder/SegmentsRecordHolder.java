package com.cut.cutz.holder;

import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;

/**
 * @author Pogorelov on 21.10.2015
 */
public class SegmentsRecordHolder {

    private Long startTime;
    private Long endTime;
    private byte[] recordInBytes;
    private CroppedTrack track;

    public Long getStartTime() {
        return startTime;
    }

    public SegmentsRecordHolder setStartTime(Long startTime) {
        this.startTime = startTime;
        return this;
    }

    public Long getEndTime() {
        return endTime;
    }

    public SegmentsRecordHolder setEndTime(Long endTime) {
        this.endTime = endTime;
        return this;
    }

    public CroppedTrack getTrack() { return track; }

    public SegmentsRecordHolder setTrack(CroppedTrack track){
        this.track = track;
        return this;
    }

//    public byte[] getRecordInBytes() {
//        return recordInBytes;
//    }
//
//    public SegmentsRecordHolder setRecordInBytes(byte[] recordInBytes) {
//        this.recordInBytes = recordInBytes;
//        return this;
//    }
}
