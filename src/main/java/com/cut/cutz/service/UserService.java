package com.cut.cutz.service;

import com.cut.cutz.db.entity.User;
import com.cut.cutz.web.dto.FollowerDto;
import com.cut.cutz.web.dto.UserDto;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

/**
 * @author Pogorelov on 03.10.2015
 */
public interface UserService {

    User save(User user);

    User findById(Integer id);

    void remove(Integer id);

    UserDto getUserDtoById(Integer id);

    List<UserDto> searchUsersDtoByQuery(String query);

    User findByEmail(String email);

    User findByEmailWithFollowers(String email);

    User findByEmailWithFollowersAndFollowing(String email);

    User findByEmailWithFollowing(String email);

    User findByIdWithGenresAndFollowing(Integer id);

    void passwordReset(String email);

    User getCurrentUser();

    void setCurrentUser(User user, HttpServletRequest req);

    UserDto followUser(Integer userId);

    UserDto updateUserData(final String username, final String email, MultipartFile photo,
                        final String newPassword, final String oldPassword) throws Exception;

    boolean doesEmailExist(String email);

    boolean doesUsernameExist(String username);

    List<FollowerDto> getSuggestedPeople();

    Set<FollowerDto> getFollowers(Integer userId);

    Set<FollowerDto> getFollowing(Integer userId);

}
