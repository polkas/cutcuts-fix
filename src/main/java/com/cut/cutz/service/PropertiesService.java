package com.cut.cutz.service;

/**
 * @author Pogorelov on 12/11/2015
 */
public interface PropertiesService {

    String getStorageServerURL();

    String getStorageFolder();

    String getLogoPath();

}
