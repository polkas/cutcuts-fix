package com.cut.cutz.service;

import com.cut.cutz.db.entity.User;
import com.cut.cutz.web.dto.MovieDto;
import com.cut.cutz.web.dto.RecordsUploadDto;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Pogorelov on 09.10.2015
 */
public interface RecordService {

    List<MovieDto> mergeRecords(RecordsUploadDto recordsUploadDto, HttpServletRequest request, final User currentUser) throws Exception;
}
