package com.cut.cutz.service;

import com.cut.cutz.db.entity.Movie;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.holder.SegmentsRecordHolder;
import com.cut.cutz.web.dto.LikeDto;
import com.cut.cutz.web.dto.LoginDto;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Pogorelov on 12/24/2015
 */
public interface IntegrationService {

    LoginDto userInformationOnLogin(String email, String password, HttpServletRequest req);

    LoginDto userInformationOnFacebookLogin(String username, String photoUrl, String email, HttpServletRequest req);

    LoginDto storeInitData(String selectedIDsComaSeparated, String followingUserIDsComaSeparated);

    void saveNewVideoForUser(HttpServletRequest request, User user, Movie oldMovie, String title,
                             List<SegmentsRecordHolder> segmentsRecordHolders) throws Exception;

    LikeDto increaseMovieLikes(Long movieId, User currentUser);
}
