package com.cut.cutz.service.impl;

import com.cut.cutz.constants.ErrorConstants;
import com.cut.cutz.db.entity.Genre;
import com.cut.cutz.db.entity.Movie;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.exception.UnexpectedDataException;
import com.cut.cutz.helper.DataHelper;
import com.cut.cutz.holder.SegmentsRecordHolder;
import com.cut.cutz.logging.InjectLogger;
import com.cut.cutz.manager.StorageManager;
import com.cut.cutz.service.*;
import com.cut.cutz.manager.MovieManager;
import com.cut.cutz.web.dto.*;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.cut.cutz.helper.DataHelper.emailValidation;

/**
 * @author Pogorelov on 12/24/2015
 */
@Service
public class IntegrationServiceImpl implements IntegrationService {

    @InjectLogger
    private Logger log;

    @Inject
    private UserService userService;
    @Inject
    private MovieService movieService;
    @Inject
    private GenreService genreService;
    @Inject
    private MovieManager movieManager;
    @Inject
    private StorageManager storageManager;

    @Override
    @Transactional
    public synchronized LoginDto userInformationOnLogin(String email, String password, HttpServletRequest req) {
        emailValidation(email);

        User user = userService.findByEmail(email);

        final LoginDto loginDto = new LoginDto();
        final LoginUserDto loginUserDto = new LoginUserDto();
        List<MovieDto> userMoviesDto;
        int following;
        int followers;
        boolean isNewUser;

        if (user != null) {
            passwordValidation(password, user);

            userMoviesDto = movieService.getMoviesDtoByUser(user);
            followers = user.getFollowers().size();
            following = user.getFollowing().size();
            isNewUser = false;
        } else {
            user = new User();
            final String userName = DataHelper.extractUserNameFromEmail(email);
            user.setEmail(email)
                    .setPassword(password)
                    .setName(userName)
                    .setPhotoUrl("");

            user = userService.save(user);
            userMoviesDto = new ArrayList<>();
            following = 0;
            followers = 0;
            isNewUser = true;
        }


        Integer userId = user.getId();

        userService.setCurrentUser(user, req);

        mainUserInformation(user, loginDto, loginUserDto, userId, userMoviesDto, followers, following, isNewUser);

        log.info("User: " + loginUserDto.getUserName() + " has logged in.");
        return loginDto;
    }

    @Override
    @Transactional
    public synchronized LoginDto userInformationOnFacebookLogin(String username, String photoUrl, String email, HttpServletRequest req) {
        final LoginDto loginDto = new LoginDto();
        final LoginUserDto loginUserDto = new LoginUserDto();
        List<MovieDto> userMoviesDto = new ArrayList<>();
        final int following = 0;
        final int followers = 0;
        boolean isNewUser = true;
        Integer userId;

        User user = userService.findByEmail(email);

        if (user == null) {
            user = new User();
            user.setEmail(email);
        } else {
            isNewUser = false;
        }

        user.setName(username)
                .setPassword(null)
                .setPhotoUrl(photoUrl);

        user = userService.save(user);

        userId = user.getId();

        userService.setCurrentUser(user, req);

        mainUserInformation(user, loginDto, loginUserDto, userId, userMoviesDto, followers, following, isNewUser);

        log.info("User: " + loginUserDto.getUserName() + " has logged in.");

        return loginDto;

    }

    @Override
    @Transactional
    public LoginDto storeInitData(String selectedIDsComaSeparated, String followingUserIDsComaSeparated) {
        User user = userService.getCurrentUser();
        user = userService.findById(user.getId());

        String[] genresIdArray = selectedIDsComaSeparated.split(",");

        List<Genre> chosenGenres = new ArrayList<>();

        for (String genreIdString : genresIdArray) {
            if (genreIdString != null && !genreIdString.isEmpty()) {
                final Integer genreId = new Integer(genreIdString);
                final Genre genre = genreService.getGenreById(genreId);
                if (genre != null) {
                    chosenGenres.add(genre);
                }
            }
        }
        user.setGenres(chosenGenres);

        user = userService.save(user);

        String[] followingUserIds = followingUserIDsComaSeparated.split(",");

        for (String followingUserId : followingUserIds) {
            if (followingUserId != null && !followingUserId.isEmpty()) {
                Integer userIdforFollow = new Integer(followingUserId);
                userService.followUser(userIdforFollow);
            }
        }

        final LoginDto loginDto = new LoginDto();
        final LoginUserDto loginUserDto = new LoginUserDto();
        final Integer userId = user.getId();

        final List<MovieDto> userMoviesDto = movieService.getMoviesDtoByUser(user);
        final Integer followers = user.getFollowers().size();
        final Integer following = user.getFollowing().size();

        mainUserInformation(user, loginDto, loginUserDto, userId, userMoviesDto, followers, following, false);

        return loginDto;
    }

    private void passwordValidation(String password, User user) {
        if (!DataHelper.isValidPassword(password, user)) {
            final String msg = ErrorConstants.BAD_CREDENTIALS;
            log.error(msg);
            throw new UnexpectedDataException(msg);
        }
    }


    @Transactional
    public void saveNewVideoForUser(HttpServletRequest request, User user, Movie oldMovie, String title,
                                    List<SegmentsRecordHolder> segmentsRecordHolders) throws Exception {
        if (user == null) return;

        final String newFileName = movieManager.mergeVideoWithSound(oldMovie.getFileName(), segmentsRecordHolders);

        final String fileUrl = storageManager.getFullMovieUrl(newFileName);

        List<Movie> movies = new ArrayList<>();

        String movieName = oldMovie.getName();

        if (title != null && !title.isEmpty()) movieName = title;


        Movie movie = new Movie();
        movie.setGenre(oldMovie.getGenre())
                .setName(movieName)
                .setFileName(newFileName)
                .setAuthor(user)
                .setViews(0)
                .setLikes(new ArrayList<>())
                .setDate(new Date())
                .setDescription(oldMovie.getDescription())
                .setMovieUrl(fileUrl)
                .setPosterUrl(oldMovie.getPosterUrl())
                .setDuration(oldMovie.getDuration())
                .setParent(oldMovie);

        movie = movieService.save(movie);

        movies.add(movie);

        List<Genre> userGenres = user.getGenres();
        if (!userGenres.contains(movie.getGenre())) {
            userGenres.add(movie.getGenre());
        }

        user.setMovies(movies)
                .setGenres(userGenres);

        userService.save(user);
    }

    @Override
    public LikeDto increaseMovieLikes(Long movieId, User currentUser) {
        Movie movie = movieService.getByMovieById(movieId);

        if (movie == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_MOVIE);
        }

        if (currentUser == null || currentUser.getEmail() == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
        }

        currentUser = userService.findByEmail(currentUser.getEmail());

        if (currentUser == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
        }

        return movieService.increaseMovieLikes(movieId, currentUser);
    }

    private void mainUserInformation(User user, LoginDto loginDto, LoginUserDto loginUserDto, Integer userId,
                                     List<MovieDto> userMoviesDto, Integer followers, Integer following,
                                     boolean isNewUser) {
        Boolean isFbUser = null;
        if (user.getPassword() == null || user.getPassword().isEmpty()) isFbUser = true;

        loginUserDto.setUserId(userId)
                .setUserName(user.getName())
                .setEmail(user.getEmail())
                .setPhotoUrl(user.getPhotoUrl())
                .setFollowers(followers)
                .setMoviesSize(userMoviesDto.size());

        loginUserDto.setUserMovies(userMoviesDto)
                .setFollowing(following)
                .setNewUser(isNewUser)
                .setFbUser(isFbUser);

        final List<MovieDto> myFeed = movieService.getMoviesDtoByFriends(user);
        final List<MovieDto> newest = movieService.getNewestMoviesDto(user);
        final List<MovieDto> popular = movieService.getPopularMoviesDto(user);
        final List<GenreDto> genresDto = genreService.getAllGenreDtosByUserId(userId);
        final List<FollowerDto> suggestedPeople = userService.getSuggestedPeople();

        loginDto.setLoginUserDto(loginUserDto)
                .setMyFeedMovies(myFeed)
                .setNewestMovies(newest)
                .setPopularMovieDto(popular)
                .setGenreDto(genresDto)
                .setSuggestedPeople(suggestedPeople);
    }

}
