package com.cut.cutz.service.impl;

import com.cut.cutz.constants.ErrorConstants;
import com.cut.cutz.db.entity.Genre;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.db.repository.GenreRepository;
import com.cut.cutz.db.repository.UserRepository;
import com.cut.cutz.exception.UnexpectedDataException;
import com.cut.cutz.service.GenreService;
import com.cut.cutz.service.UserService;
import com.cut.cutz.web.dto.GenreDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Pogorelov on 13.09.2015
 */
@Service
public class GenreServiceImpl implements GenreService {

    @Autowired
    private GenreRepository genreRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public List<GenreDto> getAllGenreDtosByUserId(Integer userId) {

        if(userId == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
        }

        List<Genre> genres = genreRepository.findAll();
        User user = userRepository.findOne(userId);

        List<Genre> userGenres = user.getGenres();

        return genres.stream().map(genre -> new GenreDto()
                .setId(genre.getId()).setGenre(genre.getName())
                .setIsSelected(userGenres.contains(genre))).collect(Collectors.toList());
    }

    @Override
    public Genre getGenreById(Integer genreId) {
        if(genreId == null) return null;

        return genreRepository.findOne(genreId);
    }
}
