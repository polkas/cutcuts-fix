package com.cut.cutz.service.impl;

import com.cut.cutz.constants.ErrorConstants;
import com.cut.cutz.db.entity.Movie;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.db.repository.UserRepository;
import com.cut.cutz.exception.UnexpectedDataException;
import com.cut.cutz.exception.UserAuthenticationException;
import com.cut.cutz.helper.DataHelper;
import com.cut.cutz.logging.InjectLogger;
import com.cut.cutz.notification.EmailSenderService;
import com.cut.cutz.service.UserService;
import com.cut.cutz.utils.FileUploader;
import com.cut.cutz.utils.LogInfo;
import com.cut.cutz.web.dto.FollowerDto;
import com.cut.cutz.web.dto.MovieDto;
import com.cut.cutz.web.dto.UserDto;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.cut.cutz.helper.DataHelper.emailValidation;

/**
 * @author Pogorelov on 03.10.2015
 */
@Service
public class UserServiceImpl implements UserService {

    @InjectLogger
    private Logger log;

    @Inject
    private UserRepository userRepository;
    @Inject
    private EmailSenderService emailSenderService;
    @Inject
    private FileUploader fileUploader;


    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findById(Integer id) {
        return userRepository.getOne(id);
    }

    @Override
    public void remove(Integer id) {
        userRepository.delete(id);
    }

    @Override
    @Transactional
    public UserDto getUserDtoById(Integer id) {
        if (id == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
        }

        final User user = userRepository.findOne(id);
        if (user == null) {
            throw new UnexpectedDataException(ErrorConstants.USER_DOES_NOT_EXIST);
        }

        final User currentUser = getCurrentUser();

        return transformUserToUserDto(user.getId(), currentUser, user);
    }

    @Override
    @Transactional
    public List<UserDto> searchUsersDtoByQuery(String query) {
        final User currentUser = getCurrentUser();
        final Integer currentUserId = currentUser.getId();
        List<User> users = userRepository.findByNameLike(currentUserId, query);

        List<UserDto> userDtos = new ArrayList<>();

        UserDto userDto;
        for (User user : users) {
            userDto = transformUserToUserDto(user.getId(), currentUser, user);

            userDtos.add(userDto);
        }

        return userDtos;
    }

    private UserDto transformUserToUserDto(Integer userId, User currentUser, User user) {
        UserDto userDto = new UserDto();

        final List<Movie> userMovies = user.getCretedMovies();

        final Integer followers = user.getFollowers().size();
        final Integer following = user.getFollowing().size();
        final List<MovieDto> movieDtos = userMovies.stream().map(movie -> new MovieDto(movie, currentUser)).collect(Collectors.toList());
//        final List<FollowerDto> following = user.getFollowing().stream().map(FollowerDto::new).collect(Collectors.toList());

        Boolean hasRelations = false;
        if (!currentUser.getId().equals(userId)) {
            hasRelations = user.getFollowers().contains(currentUser);
        }

        Boolean isFbUser = null;
        if (user.getPassword() == null || user.getPassword().isEmpty()) isFbUser = true;

        userDto.setHasRelations(hasRelations)
                .setFbUser(isFbUser)
                .setUserMovies(movieDtos)
                .setMoviesSize(movieDtos.size());

        userDto.setEmail(user.getEmail())
                .setUserId(user.getId())
                .setPhotoUrl(user.getPhotoUrl())
                .setUserName(user.getName())
                .setFollowers(followers);

        userDto.setFollowing(following);

        return userDto;
    }

    @Override
    @Transactional
    public User findByEmailWithFollowers(String email) {
        if (email == null) return null;

        User user = userRepository.findByEmail(email);

        if (user != null) {
            user.getFollowers().size();
        }

        return user;
    }

    @Override
    @Transactional
    public User findByEmailWithFollowersAndFollowing(String email) {
        if (email == null) return null;

        User user = userRepository.findByEmail(email);

        if (user != null) {
            user.getFollowing().size();
            user.getFollowers().size();
        }

        return user;
    }

    @Override
    @Transactional
    public User findByEmailWithFollowing(String email) {
        if (email == null) return null;

        User user = userRepository.findByEmail(email);

        if (user != null) {
            user.getFollowing().size();
        }

        return user;
    }

    @Override
    @Transactional
    public User findByIdWithGenresAndFollowing(Integer id) {
        if (id == null) return null;

        User user = userRepository.findOne(id);

        if (user != null) {
            user.getGenres().size();
            user.getFollowing().size();
        }

        return user;
    }

    @Transactional
    public void passwordReset(String email) {
        emailValidation(email);

        User user = userRepository.findByEmail(email);
        if (user != null) {
            final String password = RandomStringUtils.randomAlphanumeric(9);

            user.setPassword(password);

            userRepository.save(user);

            emailSenderService.sendToUser(user, password);
            log.info("Password was successfully restored. Email sent.");
        } else {
            final String msg = "User not found with with the following email.";
            log.error(msg);
            throw new UserAuthenticationException(msg);
        }
    }


    @Override
    @Transactional
    public UserDto updateUserData(final String username, final String email, MultipartFile photo,
                                  final String newPassword, final String oldPassword) throws Exception {

        User user = getCurrentUser();

        if(user == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
        }

        user = findById(user.getId());

        log.info("\n*** Update data. \n Username: %s \n email: %s \n new password: %s \n old password: %s \n",
                username, email, newPassword, oldPassword);

        if (photo != null && !photo.isEmpty()) {
            //TODO remove commented code
//            try {
//                final String folder = propertiesService.getStorageFolder() + Constants.PHOTOS_FOLDER_PATH;
//                final String fileName = DataHelper.generateRandomUUID() + Constants.PNG_EXTENSION;
//
//                log.info("Start photo processing.");
//                if (fileUploader.saveFile(photo, folder, fileName)) {
//                    final String userPhotoUrl = propertiesService.getStorageServerURL() + Constants.STORAGE_FOLDER_PATH +
//                            Constants.PHOTOS_FOLDER_PATH + fileName;
//
//                    user.setPhotoUrl(userPhotoUrl);
//                    log.info("Photo saved.");
//                } else {
//                    log.error("Failed to save photo.");
//                }
//            } catch (IOException e) {
//                log.error("Failed to save photo.", e.getMessage());
//                e.printStackTrace();
//            }
            final String userPhotoUrl = fileUploader.saveFile(photo, LogInfo.USER_PHOTO);
            user.setPhotoUrl(userPhotoUrl);
        }

        if (email != null && !email.isEmpty()) {
            log.info("Add email.");
            checkEmail(email);
            user.setEmail(email);
        }

        if (username != null && !username.isEmpty()) {
            log.info("Add username.");
            user.setName(username);
        }

        if (newPassword != null) {
            checkPassword(oldPassword, user);
            log.info("Add new password.");
            user.setPassword(newPassword);
        }

        user = userRepository.save(user);
        log.info("User data was successfully changed.");
        return transformUserToUserDto(user.getId(), getCurrentUser(), user);
    }

    private void checkPassword(String oldPassword, User user) {
        if (user.getPassword() != null &&
                (oldPassword != null && !user.getPassword().equals(DataHelper.encodedValue(oldPassword)))) {
            final String msg = "The current password is incorrect.";
            log.info(msg + " For userId: " + user.getId());
            throw new IllegalArgumentException(msg);
        }
    }

    private void checkEmail(String email) {
        emailValidation(email);

        final User user = getCurrentUser();

        //if email already exists and email does not equal to current user email
        if (doesEmailExist(email) && !user.getEmail().equals(email)) {
            final String msg = String.format("Email: %s already exists.", email);
            log.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public boolean doesEmailExist(String email) {
        final User user = userRepository.findByEmail(email);
        return user != null;
    }

    @Override
    public boolean doesUsernameExist(String username) {
        final User user = userRepository.findByName(username);
        return user != null;
    }

    @Override
    public void setCurrentUser(User user, HttpServletRequest req) {
        if (user != null && SecurityContextHolder.getContext().getAuthentication() != null
                && !user.equals(SecurityContextHolder.getContext().getAuthentication().getPrincipal())) {
            UsernamePasswordAuthenticationToken authentication =
                    new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());

            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } else if (user == null) {
            SecurityContextHolder.getContext().setAuthentication(null);
        } else if (SecurityContextHolder.getContext().getAuthentication() == null) {
            UsernamePasswordAuthenticationToken authentication =
                    new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());

            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }

    @Override
    @Transactional
    public UserDto followUser(Integer userId) {
        final User user = getCurrentUser();

        final boolean hasRelation = userRepository.followUser(user, userId);

        UserDto userDto = getUserDtoById(userId);

        userDto.setHasRelations(hasRelation);

        return userDto;
    }

    @Override
    public List<FollowerDto> getSuggestedPeople() {
        final int maxPeopleCount = 20;
        User currentUser = getCurrentUser();
        List<User> users = userRepository.findSuggestedUsersByTotalLikesOfMovies(currentUser, maxPeopleCount);

        return users.stream().map(FollowerDto::new).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Set<FollowerDto> getFollowers(Integer userId) {
        final User user = findById(userId);
        return user.getFollowers().stream().map(FollowerDto::new).collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public Set<FollowerDto> getFollowing(Integer userId) {
        final User user = findById(userId);
        return user.getFollowing().stream().map(FollowerDto::new).collect(Collectors.toSet());
    }

    public static boolean isAuthenticated() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return (principal instanceof User);
    }

    public User getCurrentUser() {
        User user = null;

        if (isAuthenticated()) {
            user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }

        return user;
    }

}
