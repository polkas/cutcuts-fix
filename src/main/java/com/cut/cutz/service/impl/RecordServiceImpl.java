package com.cut.cutz.service.impl;

import com.cut.cutz.db.entity.Movie;
import com.cut.cutz.db.entity.Segment;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.helper.DataHelper;
import com.cut.cutz.holder.SegmentsRecordHolder;
import com.cut.cutz.logging.InjectLogger;
import com.cut.cutz.service.IntegrationService;
import com.cut.cutz.service.MovieService;
import com.cut.cutz.service.RecordService;
import com.cut.cutz.web.dto.MovieDto;
import com.cut.cutz.web.dto.RecordsUploadDto;
import com.googlecode.mp4parser.MemoryDataSourceImpl;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.tracks.AACTrackImpl;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Pogorelov on 09.10.2015
 */
@Service
public class RecordServiceImpl implements RecordService {

    @InjectLogger
    private Logger log;

    @Inject
    private MovieService movieService;
    @Inject
    private IntegrationService integrationService;

    @Override
    @Transactional
    public List<MovieDto> mergeRecords(RecordsUploadDto recordsUploadDto, HttpServletRequest request, final User currentUser) throws Exception {
        final Long movieId = recordsUploadDto.getMovieId();

        log.info(String.format("\n\n*** Start merging records by user: %s and parent movieId: %d", currentUser.getUsername(), movieId));

        log.info(String.format("Record object: %s", recordsUploadDto));

        List<MultipartFile> uploadRecords = recordsUploadDto.getFiles();
        final String movieTitle = recordsUploadDto.getTitle();
        try {
            final Movie movie = movieService.getByMovieIdWithSegments(movieId);

            if(movie == null) {
                final String msg = "The movie with the following movieId:" + movieId + " does not exist.";
                throw new IllegalArgumentException(msg);
            }

            if(uploadRecords == null || uploadRecords.size() == 0) {
                final String msg = "The records could not be empty!";
                throw new IllegalArgumentException(msg);
            }

            final List<Segment> movieSegments = movie.getSegments();

            List<SegmentsRecordHolder> segmentsRecordHolders = new ArrayList<>();

            SegmentsRecordHolder srHolder;

            int recordCounter = 0;
            for (MultipartFile record : DataHelper.safe(uploadRecords)) {

                if (record.isEmpty()) {
                    ++recordCounter;
                    continue;
                }

                srHolder = new SegmentsRecordHolder();
                final Segment segment = movieSegments.get(recordCounter);

                if(segment == null) continue;

                final long startTime = segment.getStartTime();
                final long endTime = segment.getEndTime();
                final long durationTime = endTime - startTime;

                double startSec = startTime/1000;
                double endSec = endTime/1000;

                AACTrackImpl audioTrack = new AACTrackImpl(new MemoryDataSourceImpl(record.getBytes()));

                double correctedStartTime = 0;
                double correctedEndTime = -1;

                boolean timeCorrected = false;

                if (audioTrack.getSyncSamples() != null && audioTrack.getSyncSamples().length > 0) {
                    if (timeCorrected) {
                        throw new RuntimeException("The startTime has already been corrected by another track with SyncSample. Not Supported.");
                    }
                    correctedStartTime = correctTimeToSyncSample(audioTrack, startSec, false);
                    correctedEndTime = correctTimeToSyncSample(audioTrack, endSec, true);
                    timeCorrected = true;
                }

                long currentSample = 0;
                double currentTime = 0;
                double lastTime = -1;
                long startSample1 = -1;
                long endSample1 = -1;

                for (int i = 0; i < audioTrack.getSampleDurations().length; i++) {
                    long delta = audioTrack.getSampleDurations()[i];


                    if (currentTime > lastTime && currentTime <= correctedStartTime) {
                        // current sample is still before the new starttime
                        startSample1 = currentSample;
                    }
                    if (currentTime > lastTime && currentTime <= correctedEndTime) {
                        // current sample is after the new start time and still before the new endtime
                        endSample1 = currentSample;
                    }
                    lastTime = currentTime;
                    currentTime += (double) delta / (double) audioTrack.getTrackMetaData().getTimescale();
                    currentSample++;
                }

                CroppedTrack aacTrackCrop = new CroppedTrack(audioTrack, startSample1, endSample1);
                //ClippedTrack aacTrackClip = new ClippedTrack(audioTrack, 0, durationTime);

                srHolder.setStartTime(startTime)
                        .setEndTime(endTime)
                        .setTrack(aacTrackCrop);

                segmentsRecordHolders
                        .add(srHolder);


                ++recordCounter;
            }

            integrationService.saveNewVideoForUser(request, currentUser, movie, movieTitle, segmentsRecordHolders);

            log.info("\n\n*** End merging records.");
            return movieService.getMoviesDtoByUser(currentUser);
        } catch (Exception e) {
            e.printStackTrace();
            final String msg = "Failed to upload the record!" +
                    "\nError occurred:" + e.getMessage();

            log.error(msg);
            throw e;
        }
    }

    private static double correctTimeToSyncSample(Track track, double cutHere, boolean next) {
        double[] timeOfSyncSamples = new double[track.getSyncSamples().length];
        long currentSample = 0;
        double currentTime = 0;
        for (int i = 0; i < track.getSampleDurations().length; i++) {
            long delta = track.getSampleDurations()[i];

            if (Arrays.binarySearch(track.getSyncSamples(), currentSample + 1) >= 0) {
                // samples always start with 1 but we start with zero therefore +1
                timeOfSyncSamples[Arrays.binarySearch(track.getSyncSamples(), currentSample + 1)] = currentTime;
            }
            currentTime += (double) delta / (double) track.getTrackMetaData().getTimescale();
            currentSample++;

        }
        double previous = 0;
        for (double timeOfSyncSample : timeOfSyncSamples) {
            if (timeOfSyncSample > cutHere) {
                if (next) {
                    return timeOfSyncSample;
                } else {
                    return previous;
                }
            }
            previous = timeOfSyncSample;
        }
        return timeOfSyncSamples[timeOfSyncSamples.length - 1];
    }

}
