package com.cut.cutz.service.impl;

import com.cut.cutz.constants.ErrorConstants;
import com.cut.cutz.db.entity.*;
import com.cut.cutz.db.repository.CommentRepository;
import com.cut.cutz.db.repository.MovieRepository;
import com.cut.cutz.db.repository.SegmentRepository;
import com.cut.cutz.exception.UnexpectedDataException;
import com.cut.cutz.helper.DataHelper;
import com.cut.cutz.logging.InjectLogger;
import com.cut.cutz.manager.StorageManager;
import com.cut.cutz.service.GenreService;
import com.cut.cutz.service.MovieService;
import com.cut.cutz.utils.FileUploader;
import com.cut.cutz.manager.MovieManager;
import com.cut.cutz.utils.LogInfo;
import com.cut.cutz.web.dto.*;
import org.slf4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Pogorelov on 30.09.2015
 */
@Service
public class MovieServiceImpl implements MovieService {

    @Inject
    private MovieRepository movieRepository;
    @Inject
    private MovieManager movieManager;
    @Inject
    private GenreService genreService;
    @Inject
    private SegmentRepository segmentRepository;
    @Inject
    private FileUploader fileUploader;
    @Inject
    private CommentRepository commentRepository;
    @Inject
    private StorageManager storageManager;


    @InjectLogger
    private Logger log;


    @Override
    @Transactional
    public List<MovieDto> getMoviesDtoByUser(User currentUser) {
        if (currentUser == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
        }
        List<Movie> movies = movieRepository.getAllMoviesByUserId(currentUser.getId());

        return transformMoviesToMoviesDto(movies, currentUser);
    }

    @Override
    @Transactional
    public List<MovieDto> getPopularMoviesDto(User currentUser) {
        List<Movie> movies = movieRepository.getPopularMovies();

        return transformMoviesToMoviesDto(movies, currentUser);
    }

    @Override
    @Transactional
    public List<MovieDto> getNewestMoviesDto(User currentUser) {
        List<Movie> movies = movieRepository.getNewestMovies();

        return transformMoviesToMoviesDto(movies, currentUser);
    }

    @Override
    @Transactional
    public List<MovieDto> getMoviesDtoByFriends(User currentUser) {
        List<Movie> movies = movieRepository.getLastFriendsMoviesWithNumOfMovies(currentUser.getId());

        return transformMoviesToMoviesDto(movies, currentUser);
    }


    @Override
    @Transactional
    public List<MovieDto> searchMoviesByQuery(String query, User currentUser) {
        List<Movie> movies = movieRepository.findByNameLikeAndParentIdIsNotNull("%" + query + "%");

        return transformMoviesToMoviesDto(movies, currentUser);
    }

    @Override
    public void remove(Long movieId) {
//        final Movie movie = movieRepository.findById(movieId);
//        String movieUrl = movie.getMovieUrl();
//        if(movieUrl != null) {
//            final String movieName = movieUrl.substring(movieUrl.indexOf("videos/"));
//            final String storageFolder = propertiesService.getStorageFolder();
//            File file = new File(storageFolder + movieName);
//            if(file.exists()) file.delete();
//        }
        movieRepository.delete(movieId);
    }

    @Override
    @Transactional
    public List<MovieDto> getMoviesDtoByGenreId(Integer genreId, User currentUser) {
        if (genreId == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_GENRE_ID);
        }

        List<Movie> movies = movieRepository.getEditableMoviesWithNumOfMovies(genreId);

        return transformMoviesToMoviesDto(movies, currentUser);
    }

    @Override
    @Transactional
    public MovieDto getMovieDtoLikeName(String name, User currentUser) {
        if (name == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_MOVIE);
        }

        List<Movie> movies = movieRepository.findByNameLike("%" + name + "%");

        if (movies == null || movies.isEmpty()) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_MOVIE);
        }

        return transformMoviesToMoviesDto(movies, currentUser).get(0);
    }

    @Override
    @Transactional
    public PreviewComponentsDto findPreviewComponents(Long movieId, User currentUser) {
        if (movieId == null) {
            throw new UnexpectedDataException("Movie id should be present");
        }

        final Movie movie = movieRepository.findById(movieId);
        if (movie == null) {
            throw new UnexpectedDataException("Movie does not exist");
        }
        if (movie.getParent() == null) {
            throw new UnexpectedDataException("Parent id should be present");
        }

        final Long parentId = movie.getParent().getId();


        List<Movie> movies = movieRepository.findSimilarMovies(movieId, parentId);

        List<MovieDto> movieDtos = transformMoviesToMoviesDto(movies, currentUser);

        List<Comment> comments = commentRepository.findCommentsForMovieId(movieId);

        List<CommentDto> commentDtos = comments.stream().map(CommentDto::new).collect(Collectors.toList());

        PreviewComponentsDto result = new PreviewComponentsDto();
        result.setSimilarMovies(movieDtos)
                .setComments(commentDtos);

        return result;
    }

    @Override
    @Transactional
    public Movie getByMovieIdWithSegments(Long movieId) {
        if (movieId == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_MOVIE);
        }

        Movie movie = movieRepository.findById(movieId);
        if (movie != null) {
            movie.getSegments().size();
            movie.getComments().size();
            movie.getLikes().size();
        }

        return movie;
    }

    @Override
    public Movie getByMovieById(Long movieId) {
        return movieId == null ? null : movieRepository.findOne(movieId);
    }

    @Override
    @Transactional
    public MovieDto getByMovieDtoById(Long movieId, User currentUser) {
        return movieId == null ? null : new MovieDto(movieRepository.findOne(movieId),currentUser);
    }

    @Override
//    @Async
    public Integer increaseMovieViews(Long movieId) {
        Movie movie = getByMovieById(movieId);

        if (movie == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_MOVIE);
        }

        Integer views = movie.getViews();
        movie.setViews(++views);

        movieRepository.save(movie);
        return views;
    }

    @Override
    public LikeDto increaseMovieLikes(Long movieId, User currentUser) {
        Boolean isLiked = movieRepository.changeStatusMovieLike(movieId, currentUser.getId());

        int likes = movieRepository.getLikesCountByMovie(movieId);

        LikeDto likeDto = new LikeDto();
        likeDto.setLiked(isLiked)
                .setLikes(likes);

        return likeDto;
    }

    @Override
    public Movie save(Movie movie) {
        return movieRepository.save(movie);
    }

    @Override
    public boolean saveVideo(FileUploadDto fileUploadDto, HttpServletRequest request, User author) throws Exception {
        final MultipartFile file = fileUploadDto.getFile();
        final MultipartFile poster = fileUploadDto.getPosterFile();
        final String title = fileUploadDto.getName();
        final Integer genreId = fileUploadDto.getGenreId();

        if (file.isEmpty()) {
            return false;
        }

        try {
            final String movieStorageFolder = storageManager.getMovieStorage();
            final String randomFileName = DataHelper.generateRandomUUID() + ".mp4";
            final String movieUrl = storageManager.getFullMovieUrl(randomFileName);
            final Integer duration = fileUploadDto.getDuration();
            final Genre genre = genreService.getGenreById(genreId);
            final String description = fileUploadDto.getDescription();
            final List<SegmentUploadDto> segments = fileUploadDto.getSegments();

            byte[] bytes = file.getBytes();

            validation(author, genre, segments);

            final String posterUrl = fileUploader.saveFile(poster, LogInfo.POSTER);

            movieManager.cutTheSoundFromMovie(movieStorageFolder, randomFileName, bytes);

            Movie movie = new Movie();
            movie.setName(title)
                    .setFileName(randomFileName)
                    .setViews(0)
                    .setDate(new Date())
                    .setMovieUrl(movieUrl)
                    .setPosterUrl(posterUrl)
                    .setAuthor(author)
                    .setDuration(duration)
                    .setGenre(genre)
                    .setDescription(description);

            Movie savedMovie = movieRepository.save(movie);


            Segment segment;
            ActorUploadDto actorDto;
            for (SegmentUploadDto segmentDto : segments) {
                segment = new Segment();
                actorDto = segmentDto.getActorUpload();

                Long segmentStart = segmentDto.getStart();
                Long segmentEnd = segmentDto.getEnd();
                if (segmentStart == null || segmentEnd == null) {
                    throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_START_END_TIME_SEGMENT);
                }

                if (segmentStart < 200) {
                    segmentStart = 0L;
                }

                segment.setMovie(savedMovie)
                        .setStartTime(segmentStart)
                        .setEndTime(segmentEnd);

                if (actorDto != null) {
                    //TODO remove commented code
//                    final MultipartFile picture = actorDto.getPicture();
//                    final String folder = propertiesService.getStorageFolder() + Constants.PHOTOS_FOLDER_PATH;
//                    final String fileName = DataHelper.generateRandomUUID() + Constants.PNG_EXTENSION;
//
//
//                    if (picture != null && !picture.isEmpty()) {
//                        log.info("Start actor's picture processing.");
//                        if (fileUploader.saveFile(picture, folder, fileName)) {
//                            final String pictureUrl = propertiesService.getStorageServerURL() + Constants.STORAGE_FOLDER_PATH
//                                    + Constants.PHOTOS_FOLDER_PATH + fileName;
//
//                            segment.setActorPictureUrl(pictureUrl);
//                            log.info("Actor's picture saved.");
//                        } else {
//                            log.error("Failed to save actor's picture.");
//                        }
//                    }

                    final String pictureUrl = fileUploader.saveFile(actorDto.getPicture(), LogInfo.ACTOR_PHOTO);
                    segment.setActorPictureUrl(pictureUrl);
                    segment.setActorNickname(actorDto.getNickname());
                }

                segmentRepository.save(segment);
            }

        } catch (IOException e) {
            final String msg = "Failed to upload the file with name: " + title
                    + " \nError occurred:" + e.getMessage();
            throw new Exception(msg);
        }
        return true;
    }

    private void validation(User author, Genre genre, List<SegmentUploadDto> segments) {
        if (author == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
        }

        if (genre == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_GENRE_ID);
        }

        if (segments.isEmpty()) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_NUMBR_SEGMENTS);
        }
    }

    @Override
    public boolean updateVideo(FileUploadDto fileUploadDto, Long movieId, HttpServletRequest request, User author) throws Exception {
        if (author == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
        }

        Movie movie = getByMovieById(movieId);

        if (movie == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_MOVIE);
        }

        final String movieName = fileUploadDto.getName() != null ? fileUploadDto.getName().trim() : null;
        final Integer duration = fileUploadDto.getDuration();
        final Integer genreId = fileUploadDto.getGenreId();
        final String description = fileUploadDto.getDescription() != null ? fileUploadDto.getDescription().trim() : null;

        if (isNotNull(movieName)) {
            movie.setName(movieName);
        }

        if (duration > 0) {
            movie.setDuration(duration);
        }

        Genre genre = genreService.getGenreById(genreId);

        if (genre != null) {
            movie.setGenre(genre);
        }

        if (isNotNull(description)) {
            movie.setDescription(description);
        }

        for (SegmentUploadDto segmentDto : fileUploadDto.getSegments()) {
            final Long id = segmentDto.getSegmentId();
            final Long end = segmentDto.getEnd();
            Long start = segmentDto.getStart();
            final String nickname = segmentDto.getActorUpload() != null ? segmentDto.getActorUpload().getNickname() : null;

            Segment segment = segmentRepository.findOne(id);

            if (segment == null) continue;

            if (start < 200) {
                start = 0L;
            }

            if (start >= 0) {
                segment.setStartTime(start);
            }

            if (end > 0) {
                segment.setEndTime(end);
            }

            if (isNotNull(nickname)) {
                segment.setActorNickname(nickname);
            }

            if (start > duration || end > duration) {
                throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_START_END_TIME_SEGMENT);
            }

            segmentRepository.save(segment);
        }

        movieRepository.save(movie);


        return true;
    }

    private List<MovieDto> transformMoviesToMoviesDto(List<Movie> movies, User currentUser) {
        return movies.stream().map(movie -> new MovieDto(movie, currentUser)).collect(Collectors.toList());
    }

    private boolean isNotNull(String value) {
        return value != null && !value.isEmpty();
    }

}
