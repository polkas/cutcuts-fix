package com.cut.cutz.service.impl;

import com.cut.cutz.constants.ErrorConstants;
import com.cut.cutz.db.entity.Comment;
import com.cut.cutz.db.entity.Movie;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.db.repository.CommentRepository;
import com.cut.cutz.exception.UnexpectedDataException;
import com.cut.cutz.logging.InjectLogger;
import com.cut.cutz.service.CommentService;
import com.cut.cutz.service.MovieService;
import com.cut.cutz.service.UserService;
import com.cut.cutz.web.dto.CommentDto;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;


/**
 * @author Pogorelov on 13.09.2015
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Inject
    private CommentRepository commentRepository;
    @Inject
    private MovieService movieService;
    @Inject
    private UserService userService;
    @InjectLogger
    private Logger log;

    @Override
    @Transactional
    public CommentDto addComment(final User currentUser, Long movieId, String comment) throws Exception {
        Comment newComment = new Comment();
        final Movie movie = movieService.getByMovieById(movieId);

        if(currentUser == null) throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);

        final User author = userService.findById(currentUser.getId());

        if(author == null) throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);

        commentValidation(author, movie, comment);
        LocalDateTime ldt = LocalDateTime.now();
        Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
        Date res = Date.from(instant);

        log.info("\n\n\n***Comment: " + comment);
//        comment = new String(comment.getBytes("ISO-8859-1"), "UTF-8");
//        log.info("\n\n\n***Comment after new string: " + comment);

        newComment.setAuthor(author)
                .setText(comment)
                .setMovie(movie)
                .setDate(res);

        commentRepository.save(newComment);

        log.info("\n\n\n***Comment after save: " + newComment.getText());

        return new CommentDto(newComment);
    }

    @Override
    @Transactional
    public void removeComment(User author, Long movieId, Long commentId) {
        Comment comment = commentRepository.findOne(commentId);
        Movie movie = movieService.getByMovieById(movieId);

        if(movie.getComments().contains(comment) && comment.getAuthor().equals(author)) {
            commentRepository.delete(commentId);
        }
    }

    private void commentValidation(User author, Movie movie, String comment) {
        if(author == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
        }
        if(movie == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_MOVIE);
        }
        if(comment == null || comment.isEmpty()) {
            throw new UnexpectedDataException(ErrorConstants.COMMENT_REQUIRED);
        }
    }
}
