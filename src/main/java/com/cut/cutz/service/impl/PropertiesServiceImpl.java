package com.cut.cutz.service.impl;

import com.cut.cutz.service.PropertiesService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

/**
 * @author Pogorelov on 12/11/2015
 */
@Service
@PropertySource("classpath:properties/app.properties")
public class PropertiesServiceImpl implements PropertiesService {

    @Value("${storage.server}")
    private String storageServerURl;

    @Value("${storage.folder}")
    private String webappFolderStorageServer;

    @Value("${storage.logo.path}")
    private String logoFilename;

    @Override
    public String getStorageServerURL() {
        return this.storageServerURl;
    }

    @Override
    public String getStorageFolder() {
        return this.webappFolderStorageServer;
    }

    @Override
    public String getLogoPath() {
        return this.webappFolderStorageServer + this.logoFilename;
    }
}
