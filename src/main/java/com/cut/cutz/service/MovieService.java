package com.cut.cutz.service;

import com.cut.cutz.db.entity.Movie;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.web.dto.FileUploadDto;
import com.cut.cutz.web.dto.LikeDto;
import com.cut.cutz.web.dto.MovieDto;
import com.cut.cutz.web.dto.PreviewComponentsDto;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Pogorelov on 30.09.2015
 */
public interface MovieService {

    List<MovieDto> getMoviesDtoByUser(User currentUser);

    List<MovieDto> getPopularMoviesDto(User currentUser);

    List<MovieDto> getNewestMoviesDto(User currentUser);

    List<MovieDto> getMoviesDtoByFriends(User currentUser);

    List<MovieDto> searchMoviesByQuery(String query, User currentUser);

    List<MovieDto> getMoviesDtoByGenreId(Integer genreId, User currentUser);

    MovieDto getMovieDtoLikeName(String name, User currentUser);

    PreviewComponentsDto findPreviewComponents(Long movieId, User currentUser);

    void remove(Long movieId);

//    MoviePreviewDto getMoviePreviewDto(Long userId);

    Movie getByMovieIdWithSegments(Long movieId);

    Movie getByMovieById(Long movieId);

    MovieDto getByMovieDtoById(Long movieId, User currentUser);

    Integer increaseMovieViews(Long movieId);

    LikeDto increaseMovieLikes(Long movieId, User currentUser);

//    void decreaseMovieLikes(Long movieId, Integer currentUserId);

    boolean saveVideo(FileUploadDto fileUploadDto, HttpServletRequest request, User author) throws Exception;

    boolean updateVideo(FileUploadDto fileUploadDto, Long movieId, HttpServletRequest request, User author) throws Exception;

    Movie save(Movie movie);
}
