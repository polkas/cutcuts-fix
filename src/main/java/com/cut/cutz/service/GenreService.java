package com.cut.cutz.service;

import com.cut.cutz.db.entity.Genre;
import com.cut.cutz.web.dto.GenreDto;

import java.util.List;

/**
 * @author Pogorelov on 13.09.2015
 */
public interface GenreService {

    List<GenreDto> getAllGenreDtosByUserId(Integer userId);

    Genre getGenreById(Integer genreId);
}
