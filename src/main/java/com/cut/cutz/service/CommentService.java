package com.cut.cutz.service;


import com.cut.cutz.db.entity.User;
import com.cut.cutz.web.dto.CommentDto;

/**
 * @author Pogorelov on 13.09.2015
 */
public interface CommentService {

    CommentDto addComment(final User currentUser, Long movieId, String comment) throws Exception;

    void removeComment(User author, Long movieId, Long commentId);

}
