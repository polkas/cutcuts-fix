package com.cut.cutz.config;

import com.cut.cutz.logging.FlywayStatusLogger;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Pogorelov on 09.09.2015
 */
@Configuration
@EnableJpaRepositories("com.cut.cutz.db.repository")
@EnableTransactionManagement
public class DatabaseConfiguration implements EnvironmentAware {

    private RelaxedPropertyResolver propertyResolver;
    private Environment environment;


    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
        this.propertyResolver = new RelaxedPropertyResolver(environment, "spring.datasource.");
    }


    @Bean(destroyMethod = "shutdown")
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();

        config.setDataSourceClassName(propertyResolver.getProperty("dataSourceClassName"));
        config.addDataSourceProperty("url", propertyResolver.getProperty("url"));
        config.addDataSourceProperty("user", propertyResolver.getProperty("username"));
        config.addDataSourceProperty("password", propertyResolver.getProperty("password"));

        //MySQL optimizations, see https://github.com/brettwooldridge/HikariCP/wiki/MySQL-Configuration
        if ("com.mysql.jdbc.jdbc2.optional.MysqlDataSource".equals(propertyResolver.getProperty("dataSourceClassName"))) {
            config.addDataSourceProperty("cachePrepStmts", propertyResolver.getProperty("cachePrepStmts", "true"));
            config.addDataSourceProperty("prepStmtCacheSize", propertyResolver.getProperty("prepStmtCacheSize", "250"));
            config.addDataSourceProperty("prepStmtCacheSqlLimit", propertyResolver.getProperty("prepStmtCacheSqlLimit", "2048"));
            config.addDataSourceProperty("useServerPrepStmts", propertyResolver.getProperty("useServerPrepStmts", "true"));
        }
        return new HikariDataSource(config);
    }


    @Bean(name = {"org.springframework.boot.autoconfigure.AutoConfigurationUtils.basePackages"})
    public List<String> getBasePackages() {
        List<String> basePackages = new ArrayList<>();
        basePackages.add("com.cut.cutz.db.entity");
        return basePackages;
    }

    @Bean(initMethod = "migrate")
    @ConditionalOnProperty(prefix = "flyway", name = "enabled", matchIfMissing = true)
    public Flyway flyway(DataSource dataSource) {

        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setValidateOnMigrate(false);
        flyway.setBaselineOnMigrate(true);
        return flyway;
    }

    @Bean(initMethod = "logStatus")
    @ConditionalOnProperty(prefix = "flyway", name = "enabled", matchIfMissing = true)
    public FlywayStatusLogger flywayStatusLogger(Flyway flyway) {
        return new FlywayStatusLogger(flyway);
    }
}
