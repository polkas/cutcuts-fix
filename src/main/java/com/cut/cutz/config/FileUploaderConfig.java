package com.cut.cutz.config;

import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.MultipartConfigElement;

/**
 * @author Pogorelov on 08.10.2015
 */
@Configuration
public class FileUploaderConfig {

    //100mb
    private static final Long UPLOAD_MAX_FILE_SIZE = 100L * 1024 * 1024;

//    @Bean
//    public CommonsMultipartResolver commonsMultipartResolver() {
//        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
//        resolver.setDefaultEncoding("UTF-8");
//        resolver.setMaxUploadSize(UPLOAD_MAX_FILE_SIZE);
//        return resolver;
//    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(UPLOAD_MAX_FILE_SIZE);
        factory.setMaxRequestSize(UPLOAD_MAX_FILE_SIZE);
        return factory.createMultipartConfig();
    }

}
