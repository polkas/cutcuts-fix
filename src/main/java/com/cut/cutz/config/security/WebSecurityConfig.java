package com.cut.cutz.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import static com.cut.cutz.constants.RestConstants.*;

/**
 * @author Pogorelov on 10.09.2015
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private RestAuthenticationEntryPoint authenticationEntryPoint;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/styles/**", "/scripts/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers(BASE_URL,
                            API_URL + REST_URL + LOGIN_URL + "/**",
                            API_URL + REST_URL + FACEBOOK_LOGIN_URL + "/**",
                            API_URL + REST_URL + RECOVER_PASSWORD_URL + "/**").permitAll()
                        .anyRequest().authenticated()
                .and()
                    .csrf().disable()
                        .rememberMe()//.tokenValiditySeconds(1)
                .and()
                    .logout().logoutUrl(API_URL + REST_URL + LOGOUT_URL).permitAll()
                        .logoutSuccessUrl(BASE_URL)
                            .invalidateHttpSession(true).deleteCookies("JSESSIONID")
                .and()
                    .exceptionHandling()
                        .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                    .sessionManagement()
                        .sessionCreationPolicy(SessionCreationPolicy.ALWAYS);
    }
}
