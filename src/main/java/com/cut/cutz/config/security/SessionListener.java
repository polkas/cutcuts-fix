package com.cut.cutz.config.security;

import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author Pogorelov on 9/18/2016
 */
@Configuration
public class SessionListener implements HttpSessionListener {

    //1 year = 365 d * 24 h = 8760 hours
    private final int ONE_YEAR = 8760 * 60 * 60;

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().setMaxInactiveInterval(ONE_YEAR);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
    }
}
