package com.cut.cutz.web.base;

import com.cut.cutz.constants.RestConstants;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Pogorelov on 08.09.2015
 */
@RequestMapping(RestConstants.API_URL + RestConstants.REST_URL)
public abstract class BaseRestController extends ErrorHandlerRestService {
}
