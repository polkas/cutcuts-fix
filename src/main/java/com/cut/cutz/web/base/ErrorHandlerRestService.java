package com.cut.cutz.web.base;

import com.cut.cutz.exception.UnexpectedDataException;
import com.cut.cutz.exception.UserAuthenticationException;
import com.cut.cutz.logging.InjectLogger;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author Pogorelov on 11.09.2015
 */
@Controller
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ErrorHandlerRestService {

    @InjectLogger
    private Logger log;

     @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public HashMap<String, Object> handleException(final Exception  e) throws IOException {

        HashMap<String, Object> result = new HashMap<>();

        result.put("error", true);
        result.put("error_message", e.getMessage());

        if(!(e instanceof UserAuthenticationException || e instanceof UnexpectedDataException)) {
            e.printStackTrace();
        }

        log.error("Unexpected error occurs: ", e.getMessage());

        return result;
    }
}
