package com.cut.cutz.web.dto;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Pogorelov on 09.10.2015
 */
public class RecordsUploadDto {

    private Long movieId;

    private String title;

    private List<MultipartFile> files;

    public Long getMovieId() {
        return movieId;
    }

    public RecordsUploadDto setMovieId(Long movieId) {
        this.movieId = movieId;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public RecordsUploadDto setTitle(String title) {
        this.title = title;
        return this;
    }

    public List<MultipartFile> getFiles() {
        return files;
    }

    public RecordsUploadDto setFiles(List<MultipartFile> files) {
        this.files = files;
        return this;
    }

    @Override
    public String toString() {
        return "RecordsUploadDto{" +
                "movieId=" + movieId +
                ", title='" + title + '\'' +
                ", files=" + files +
                '}';
    }
}
