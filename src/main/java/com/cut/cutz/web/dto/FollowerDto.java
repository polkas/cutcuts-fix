package com.cut.cutz.web.dto;


import com.cut.cutz.constants.ErrorConstants;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.exception.UnexpectedDataException;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Pogorelov on 11.09.2015
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class FollowerDto {

    private Integer userId;

    private String userName;

    private String email;

    private String photoUrl;

    private Integer moviesSize;

    private Integer followers;

    private Boolean hasRelations;

    public FollowerDto() {

    }

    public FollowerDto(User user) {
        if(user == null) {
            throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
        }

        this.userId = user.getId();
        this.userName = user.getName();
        this.email = user.getEmail();
        this.photoUrl = user.getPhotoUrl();
        this.moviesSize = user.getCretedMovies().size();
        this.followers = user.getFollowers().size();
    }

    public Integer getUserId() {
        return userId;
    }

    public FollowerDto setUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public FollowerDto setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public FollowerDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public FollowerDto setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
        return this;
    }

    public Integer getMoviesSize() {
        return moviesSize;
    }

    public FollowerDto setMoviesSize(Integer moviesSize) {
        this.moviesSize = moviesSize;
        return this;
    }

    public Integer getFollowers() {
        return followers;
    }

    public FollowerDto setFollowers(Integer followers) {
        this.followers = followers;
        return this;
    }

    public Boolean getHasRelations() {
        return hasRelations;
    }

    public FollowerDto setHasRelations(Boolean hasRelations) {
        this.hasRelations = hasRelations;
        return this;
    }
}
