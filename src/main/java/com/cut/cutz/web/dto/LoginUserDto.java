package com.cut.cutz.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

import static com.cut.cutz.constants.Constants.USER_MOVIES_JSON;

/**
 * @author Pogorelov on 10.11.2015
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LoginUserDto extends FollowerDto {

    private Integer following;

    @JsonProperty(USER_MOVIES_JSON)
    private List<MovieDto> userMovies;

    private Boolean isNewUser;

    private Boolean isFbUser;


    public List<MovieDto> getUserMovies() {
        return userMovies;
    }

    public LoginUserDto setUserMovies(List<MovieDto> userMovies) {
        this.userMovies = userMovies;
        return this;
    }

    public Integer getFollowing() {
        return following;
    }

    public LoginUserDto setFollowing(Integer following) {
        this.following = following;
        return this;
    }

    public Boolean getNewUser() {
        return isNewUser;
    }

    public LoginUserDto setNewUser(Boolean newUser) {
        isNewUser = newUser;
        return this;
    }

    public Boolean getFbUser() {
        return isFbUser;
    }

    public LoginUserDto setFbUser(Boolean fbUser) {
        isFbUser = fbUser;
        return this;
    }
}
