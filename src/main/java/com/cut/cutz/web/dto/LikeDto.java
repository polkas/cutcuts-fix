package com.cut.cutz.web.dto;

/**
 * @author Pogorelov on 19.02.2016
 */
public class LikeDto {

    private Integer likes;

    private boolean isLiked;

    public Integer getLikes() {
        return likes;
    }

    public LikeDto setLikes(Integer likes) {
        this.likes = likes;
        return this;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public LikeDto setLiked(boolean liked) {
        isLiked = liked;
        return this;
    }
}
