package com.cut.cutz.web.dto;

import com.cut.cutz.db.entity.Comment;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.exception.UnexpectedDataException;

import java.util.Date;

/**
 * @author Pogorelov on 12/29/2015
 */
public class CommentDto {

    private Long commentId;
    private Integer authorId;
    private Integer authorFollowersAmount;
    private Integer authorMoviesAmount;
    private String authorName;
    private String photoUrl;
    private String text;
    private Date date;

    public CommentDto(Comment comment) {
        User author = comment.getAuthor();
        if (author == null) {
            throw new UnexpectedDataException("Author should be present.");
        }
        this.commentId = comment.getId();
        this.authorId = author.getId();
        this.authorName = author.getName();
        this.authorFollowersAmount = author.getFollowers().size();
        this.authorMoviesAmount = author.getMovies().size();
        this.photoUrl = author.getPhotoUrl();
        this.text = comment.getText();
        this.date = comment.getDate();
    }

    public Long getCommentId() {
        return commentId;
    }

    public CommentDto setCommentId(Long commentId) {
        this.commentId = commentId;
        return this;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public CommentDto setAuthorId(Integer authorId) {
        this.authorId = authorId;
        return this;
    }

    public String getAuthorName() {
        return authorName;
    }

    public CommentDto setAuthorName(String authorName) {
        this.authorName = authorName;
        return this;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public CommentDto setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
        return this;
    }

    public String getText() {
        return text;
    }

    public CommentDto setText(String text) {
        this.text = text;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public CommentDto setDate(Date date) {
        this.date = date;
        return this;
    }

    public Integer getAuthorFollowersAmount() {
        return authorFollowersAmount;
    }

    public CommentDto setAuthorFollowersAmount(Integer authorFollowersAmount) {
        this.authorFollowersAmount = authorFollowersAmount;
        return this;
    }

    public Integer getAuthorMoviesAmount() {
        return authorMoviesAmount;
    }

    public CommentDto setAuthorMoviesAmount(Integer authorMoviesAmount) {
        this.authorMoviesAmount = authorMoviesAmount;
        return this;
    }
}
