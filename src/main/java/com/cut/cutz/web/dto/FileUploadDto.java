package com.cut.cutz.web.dto;

import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pogorelov on 09.10.2015
 */
public class FileUploadDto {

    private String name;

    private MultipartFile file;

    private MultipartFile posterFile;

    private Integer genreId;

    private Integer duration;

    private List<SegmentUploadDto> segments;

    private String description;


    public String getName() {
        return name;
    }

    public FileUploadDto setName(String name) {
        this.name = name;
        return this;
    }

    public MultipartFile getFile() {
        return file;
    }

    public FileUploadDto setFile(MultipartFile file) {
        this.file = file;
        return this;
    }

    public Integer getGenreId() {
        return genreId;
    }

    public FileUploadDto setGenreId(Integer genreId) {
        this.genreId = genreId;
        return this;
    }

    public Integer getDuration() {
        return duration;
    }

    public FileUploadDto setDuration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public List<SegmentUploadDto> getSegments() {
        if(segments == null) segments = new ArrayList<>();
        return segments;
    }

    public FileUploadDto setSegments(List<SegmentUploadDto> segments) {
        this.segments = segments;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public FileUploadDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public MultipartFile getPosterFile() {
        return posterFile;
    }

    public FileUploadDto setPosterFile(MultipartFile posterFile) {
        this.posterFile = posterFile;
        return this;
    }
}
