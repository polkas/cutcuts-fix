package com.cut.cutz.web.dto;

/**
 * @author Pogorelov on 10.11.2015
 */
public class SegmentUploadDto {

    private Long segmentId;

    private Long start;

    private Long end;

    private ActorUploadDto actorUpload;


    public ActorUploadDto getActorUpload() {
        return actorUpload;
    }

    public Long getSegmentId() {
        return segmentId;
    }

    public SegmentUploadDto setSegmentId(Long segmentId) {
        this.segmentId = segmentId;
        return this;
    }

    public SegmentUploadDto setActorUpload(ActorUploadDto actorUpload) {
        this.actorUpload = actorUpload;
        return this;
    }

    public Long getStart() {
        return start;
    }

    public SegmentUploadDto setStart(Long start) {
        this.start = start;
        return this;
    }

    public Long getEnd() {
        return end;
    }

    public SegmentUploadDto setEnd(Long end) {
        this.end = end;
        return this;
    }
}
