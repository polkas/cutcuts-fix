package com.cut.cutz.web.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

import static com.cut.cutz.constants.Constants.*;

/**
 * @author Pogorelov on 11.09.2015
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LoginDto {

    @JsonProperty(USER_JSON)
    private LoginUserDto loginUserDto;

    @JsonProperty(MYFEED_MOVIES_JSON)
    private List<MovieDto> myFeedMovies;

    @JsonProperty(NEWEST_MOVIES_JSON)
    private List<MovieDto> newestMovies;

    @JsonProperty(POPULAR_MOVIES_JSON)
    private List<MovieDto> popularMovieDto;

    @JsonProperty(GENRES_JSON)
    private List<GenreDto> genreDto;

    @JsonProperty(SUGGESTED_USERS_JSON)
    private List<FollowerDto> suggestedPeople;

    public LoginUserDto getLoginUserDto() {
        return loginUserDto;
    }

    public LoginDto setLoginUserDto(LoginUserDto loginUserDto) {
        this.loginUserDto = loginUserDto;
        return this;
    }

    public List<MovieDto> getMyFeedMovies() {
        return myFeedMovies;
    }

    public LoginDto setMyFeedMovies(List<MovieDto> myFeedMovies) {
        this.myFeedMovies = myFeedMovies;
        return this;
    }

    public List<MovieDto> getNewestMovies() {
        return newestMovies;
    }

    public LoginDto setNewestMovies(List<MovieDto> newestMovies) {
        this.newestMovies = newestMovies;
        return this;
    }

    public List<MovieDto> getPopularMovieDto() {
        return popularMovieDto;
    }

    public LoginDto setPopularMovieDto(List<MovieDto> popularMovieDto) {
        this.popularMovieDto = popularMovieDto;
        return this;
    }

    public List<GenreDto> getGenreDto() {
        return genreDto;
    }

    public LoginDto setGenreDto(List<GenreDto> genreDto) {
        this.genreDto = genreDto;
        return this;
    }

    public List<FollowerDto> getSuggestedPeople() {
        return suggestedPeople;
    }

    public LoginDto setSuggestedPeople(List<FollowerDto> suggestedPeople) {
        this.suggestedPeople = suggestedPeople;
        return this;
    }
}
