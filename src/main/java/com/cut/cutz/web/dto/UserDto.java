package com.cut.cutz.web.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;


/**
 * @author Pogorelov on 03.10.2015
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class UserDto extends LoginUserDto {

    private Boolean hasRelations;

    public Boolean getHasRelations() {
        return hasRelations;
    }

    public UserDto setHasRelations(Boolean hasRelations) {
        this.hasRelations = hasRelations;
        return this;
    }
}
