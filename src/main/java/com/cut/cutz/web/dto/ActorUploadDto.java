package com.cut.cutz.web.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Pogorelov on 06.10.2015
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class ActorUploadDto {

    private String nickname;

    private MultipartFile picture;

    public String getNickname() {
        return nickname;
    }

    public ActorUploadDto setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public MultipartFile getPicture() {
        return picture;
    }

    public ActorUploadDto setPicture(MultipartFile picture) {
        this.picture = picture;
        return this;
    }
}
