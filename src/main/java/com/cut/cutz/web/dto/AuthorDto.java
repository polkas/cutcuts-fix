package com.cut.cutz.web.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Pogorelov on 06.10.2015
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class AuthorDto extends FollowerDto {

}
