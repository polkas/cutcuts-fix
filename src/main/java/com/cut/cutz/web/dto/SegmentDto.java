package com.cut.cutz.web.dto;

/**
 * @author Pogorelov on 10.11.2015
 */
public class SegmentDto {
    private Long id;
    private Long start;
    private Long end;
    private ActorDto actor;

    public Long getId() {
        return id;
    }

    public SegmentDto setId(Long id) {
        this.id = id;
        return this;
    }

    public ActorDto getActor() {
        return actor;
    }

    public SegmentDto setActor(ActorDto actor) {
        this.actor = actor;
        return this;
    }

    public Long getStart() {
        return start;
    }

    public SegmentDto setStart(Long start) {
        this.start = start;
        return this;
    }

    public Long getEnd() {
        return end;
    }

    public SegmentDto setEnd(Long end) {
        this.end = end;
        return this;
    }
}
