package com.cut.cutz.web.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Pogorelov on 06.10.2015
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class ActorDto {

    private String nickname;

    private String photoUrl;

    public String getNickname() {
        return nickname;
    }

    public ActorDto setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public String getPictureUrl() {
        return photoUrl;
    }

    public ActorDto setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
        return this;
    }
}
