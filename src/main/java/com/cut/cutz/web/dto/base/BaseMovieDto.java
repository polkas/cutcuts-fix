package com.cut.cutz.web.dto.base;

import com.cut.cutz.db.entity.Segment;
import com.cut.cutz.web.dto.ActorDto;
import com.cut.cutz.web.dto.SegmentDto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Pogorelov on 30.09.2015
 */
public class BaseMovieDto {

    private Long id;
    private String name;
    private Integer views;
    private Integer likes;
    private boolean isLiked;
    private Integer duration;
    private Date date;
    private String posterUrl;
    private String movieUrl;
    private String genre;
    private String description;
    private List<SegmentDto> segments;

    public Long getId() {
        return id;
    }

    public BaseMovieDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public BaseMovieDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public BaseMovieDto setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
        return this;
    }

    public String getMovieUrl() {
        return movieUrl;
    }

    public BaseMovieDto setMovieUrl(String movieUrl) {
        this.movieUrl = movieUrl;
        return this;
    }

    public Integer getDuration() {
        return duration;
    }

    public BaseMovieDto setDuration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public String getGenre() {
        return genre;
    }

    public BaseMovieDto setGenre(String genre) {
        this.genre = genre;
        return this;
    }

    public Integer getViews() {
        return views;
    }

    public BaseMovieDto setViews(Integer views) {
        this.views = views;
        return this;
    }

    public Integer getLikes() {
        return likes;
    }

    public BaseMovieDto setLikes(Integer likes) {
        this.likes = likes;
        return this;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public BaseMovieDto setLiked(boolean liked) {
        isLiked = liked;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public BaseMovieDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public BaseMovieDto setDate(Date date) {
        this.date = date;
        return this;
    }

    public List<SegmentDto> getSegments() {
        return segments;
    }

    public BaseMovieDto setSegments(List<Segment> segments) {
        List<SegmentDto> segmentDtos = new ArrayList<>();

        SegmentDto segmentDto;
        ActorDto actorDto;
        if(segments != null) {
            for (Segment segment : segments) {
                if (segment != null) {
                    actorDto = new ActorDto();
                    actorDto.setNickname(segment.getActorNickname())
                            .setPhotoUrl(segment.getActorPictureUrl());

                    segmentDto = new SegmentDto()
                            .setId(segment.getId())
                            .setEnd(segment.getEndTime())
                            .setStart(segment.getStartTime())
                            .setActor(actorDto);

                    segmentDtos.add(segmentDto);
                }
            }
        }
        this.segments = segmentDtos;
        return this;
    }

}
