package com.cut.cutz.web.dto;


import java.util.List;

/**
 * @author Pogorelov on 1/8/2016
 */
public class PreviewComponentsDto {

    public List<MovieDto> similarMovies;

    public List<CommentDto> comments;

    public List<MovieDto> getSimilarMovies() {
        return similarMovies;
    }

    public PreviewComponentsDto setSimilarMovies(List<MovieDto> similarMovies) {
        this.similarMovies = similarMovies;
        return this;
    }

    public List<CommentDto> getComments() {
        return comments;
    }

    public PreviewComponentsDto setComments(List<CommentDto> comments) {
        this.comments = comments;
        return this;
    }
}
