package com.cut.cutz.web.dto;

import com.cut.cutz.db.entity.Segment;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Pogorelov on 20.10.2015
 */
public class UploadFileElementDto {

    private Long startTime;
    private Long endTime;
    private MultipartFile file;

    public UploadFileElementDto setStartTime(Long startTime) {
        this.startTime = startTime;
        return this;
    }

    public UploadFileElementDto setEndTime(Long endTime) {
        this.endTime = endTime;
        return this;
    }

    public MultipartFile getFile() {
        return file;
    }

    public UploadFileElementDto setFile(MultipartFile file) {
        this.file = file;
        return this;
    }

    public Segment getSegment() {
        Segment currentSegment = new Segment()
                .setStartTime(this.startTime)
                .setEndTime(this.endTime);
        return currentSegment;
    }
}
