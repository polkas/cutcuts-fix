package com.cut.cutz.web.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Pogorelov on 11.09.2015
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class GenreDto {

    private Integer id;
    private String genre;
    private Boolean isSelected;


    public String getGenre() {
        return genre;
    }

    public GenreDto setGenre(String genre) {
        this.genre = genre;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public GenreDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public Boolean getIsSelected() {
        return isSelected;
    }

    public GenreDto setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
        return this;
    }
}
