package com.cut.cutz.web.dto;

import com.cut.cutz.db.entity.Movie;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.exception.UnexpectedDataException;
import com.cut.cutz.web.dto.base.BaseMovieDto;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Pogorelov on 16.10.2015
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class MovieDto extends BaseMovieDto {

    private AuthorDto author;

    private BaseMovieDto parentMovie;

    public MovieDto(Movie movie, User currentUser) {
        if (movie == null) {
            throw new UnexpectedDataException("Movie should be present.");
        }

        final User author = movie.getAuthor();

        if (author == null) {
            throw new UnexpectedDataException("Author should be present for movie with id: " + movie.getId());
        }

        Boolean hasRelations = false;
        if (!currentUser.getId().equals(author.getId())) {
            hasRelations = author.getFollowers().contains(currentUser);
        }

        AuthorDto authorDto = new AuthorDto();
        authorDto.setUserId(author.getId())
                .setMoviesSize(author.getCretedMovies().size())
                .setUserName(author.getName())
                .setFollowers(author.getFollowers().size())
                .setPhotoUrl(author.getPhotoUrl())
                .setHasRelations(hasRelations);

        this.author = authorDto;

        Movie parent = movie.getParent();
        if (parent != null) {
            BaseMovieDto parentMovieObj = new BaseMovieDto();
            parentMovieObj.setId(parent.getId())
                    .setName(parent.getName())
                    .setSegments(parent.getSegments())
                    .setDuration(parent.getDuration())
                    .setDate(parent.getDate())
                    .setPosterUrl(parent.getPosterUrl())
                    .setMovieUrl(parent.getMovieUrl())
                    .setGenre(parent.getGenre().getName())
                    .setDescription(parent.getDescription());

            this.setParentMovie(parentMovieObj);
        }

        boolean isLiked = movie.getLikeUserIds().contains(currentUser.getId());

        this.setId(movie.getId())
                .setName(movie.getName())
                .setViews(movie.getViews())
                .setSegments(movie.getSegments())
                .setLikes(movie.getLikes().size())
                .setLiked(isLiked)
                .setDuration(movie.getDuration())
                .setDate(movie.getDate())
                .setPosterUrl(movie.getPosterUrl())
                .setMovieUrl(movie.getMovieUrl())
                .setGenre(movie.getGenre().getName())
                .setDescription(movie.getDescription());
    }

    public AuthorDto getAuthor() {
        return author;
    }

    public MovieDto setAuthor(AuthorDto author) {
        this.author = author;
        return this;
    }

    public BaseMovieDto getParentMovie() {
        return parentMovie;
    }

    public MovieDto setParentMovie(BaseMovieDto parentMovie) {
        this.parentMovie = parentMovie;
        return this;
    }
}
