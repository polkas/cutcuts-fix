package com.cut.cutz.web.controllers;

import com.cut.cutz.service.MovieService;
import com.cut.cutz.service.UserService;
import com.cut.cutz.web.base.BaseRestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

import static com.cut.cutz.constants.RestConstants.*;

/**
 * @author Pogorelov on 11/16/2015
 */
@RestController
public class DevelopmentController extends BaseRestController {

    @Inject
    private MovieService movieService;
    @Inject
    private UserService userService;

    @RequestMapping(value = DEVELOPMENT_GROUP + MOVIE_URL + "/{movieId}", method = RequestMethod.DELETE)
    public void removeMovie(@PathVariable Long movieId) {
        movieService.remove(movieId);
    }

    @RequestMapping(value = DEVELOPMENT_GROUP + USERS_URL + "/{userId}", method = RequestMethod.DELETE)
    public void removeMovie(@PathVariable Integer userId) {
        userService.remove(userId);
    }
}
