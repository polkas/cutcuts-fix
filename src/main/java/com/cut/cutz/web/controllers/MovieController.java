package com.cut.cutz.web.controllers;

import com.cut.cutz.db.entity.User;
import com.cut.cutz.logging.InjectLogger;
import com.cut.cutz.service.*;
import com.cut.cutz.web.base.BaseRestController;
import com.cut.cutz.web.dto.*;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.cut.cutz.constants.RestConstants.*;

/**
 * @author Pogorelov on 11.09.2015
 */
@RestController
public class MovieController extends BaseRestController {

    @Inject
    private MovieService movieService;
    @Inject
    private RecordService recordService;
    @Inject
    private UserService userService;
    @Inject
    private CommentService commentService;
    @Inject
    private IntegrationService integrationService;

    @InjectLogger
    private Logger log;

    @RequestMapping(value = MOVIE_URL + "/{movieId}" + VIEWS_URL, method = RequestMethod.POST)
    public @ResponseBody String increaseMovieViews(@PathVariable Long movieId) {
        final Integer movieViews = movieService.increaseMovieViews(movieId);
        return String.format("{\"views\": \"%d\"}", movieViews);
    }

    @RequestMapping(value = MOVIE_URL + "/{movieId}" + LIKES_URL, method = RequestMethod.POST)
    public @ResponseBody LikeDto increaseMovieLikes(@PathVariable Long movieId) {
        return integrationService.increaseMovieLikes(movieId, userService.getCurrentUser());
    }

    @RequestMapping(value = MOVIE_URL + "/{movieId}" + PREVIEW_COMPONENTS_URL, method = RequestMethod.GET)
    public @ResponseBody PreviewComponentsDto findPreviewComponentsWithSimilarData(@PathVariable Long movieId) {
        return movieService.findPreviewComponents(movieId, userService.getCurrentUser());
    }

    @RequestMapping(value = MOVIE_URL + "/{movieId}" + COMMENTS_URL, method = RequestMethod.POST)
    public @ResponseBody CommentDto storeCommentForMovie(@PathVariable Long movieId,  @RequestParam(COMMENT_PARAM) String comment,
                                     HttpServletRequest request)
            throws Exception {
        final User author = userService.getCurrentUser();
        return commentService.addComment(author, movieId, comment);
    }

    @RequestMapping(value = MOVIE_URL + "/{movieId}" + COMMENTS_URL + "/{commentId}", method = RequestMethod.DELETE)
    public void deleteCommentForMovie(@PathVariable Long movieId, @PathVariable Long commentId)
            throws Exception {
        final User author = userService.getCurrentUser();
        commentService.removeComment(author, movieId, commentId);
    }

    @RequestMapping(value = MOVIE_URL + GENRE_URL + "/{genreId}", method = RequestMethod.GET)
    public @ResponseBody List<MovieDto> getEditableMoviesByGenre(@PathVariable Integer genreId) {
        return movieService.getMoviesDtoByGenreId(genreId, userService.getCurrentUser());
    }

    @RequestMapping(value = MOVIE_URL + POPULAR_URL, method = RequestMethod.GET)
    public @ResponseBody List<MovieDto> getPopularMovies() {
        return movieService.getPopularMoviesDto(userService.getCurrentUser());
    }

    @RequestMapping(value = MOVIE_URL + FEEDS_URL, method = RequestMethod.GET)
    public @ResponseBody List<MovieDto> getFeeds() {
        User currentUser = userService.getCurrentUser();
        return movieService.getMoviesDtoByFriends(currentUser);
    }

    @RequestMapping(value = MOVIE_URL + NEWSET_URL, method = RequestMethod.GET)
    public @ResponseBody List<MovieDto> getNewestMovies() {
        return movieService.getNewestMoviesDto(userService.getCurrentUser());
    }

    @RequestMapping(value = MOVIE_URL + SEARCH_URL, method = RequestMethod.GET)
    public @ResponseBody List<MovieDto> searchMovies(@RequestParam(QUERY_PARAM) String query) {
        return movieService.searchMoviesByQuery(query, userService.getCurrentUser());
    }

    @RequestMapping(value = MOVIE_URL + MERGE_RECORDS_URL, method = RequestMethod.POST, consumes = { "multipart/mixed", "multipart/form-data" })
    public @ResponseBody List<MovieDto> mergeRecords(RecordsUploadDto recordsUploadDto, HttpServletRequest request)
            throws Exception {

        return recordService.mergeRecords(recordsUploadDto, request, userService.getCurrentUser());
    }

    @RequestMapping(value = MOVIE_URL + UPLOAD_URL, method = RequestMethod.POST, consumes = { "multipart/mixed", "multipart/form-data" })
    public @ResponseBody boolean uploadVideo(FileUploadDto fileUploadDto, HttpServletRequest request) throws Exception {
        User currentUser = userService.getCurrentUser();
        return movieService.saveVideo(fileUploadDto, request, currentUser);
    }

    @RequestMapping(value = MOVIE_URL + "/{movieId}" + UPDATE_URL, method = RequestMethod.POST, consumes = { "multipart/mixed", "multipart/form-data" })
    public @ResponseBody boolean updateVideo(FileUploadDto fileUploadDto, @PathVariable Long movieId, HttpServletRequest request) throws Exception {
        User currentUser = userService.getCurrentUser();
        return movieService.updateVideo(fileUploadDto, movieId, request, currentUser);
    }

    @RequestMapping(value = MOVIE_URL + EDIT_URL, method = RequestMethod.GET)
    public @ResponseBody MovieDto searchMovie(@RequestParam(QUERY_PARAM) String name) {
        return movieService.getMovieDtoLikeName(name, userService.getCurrentUser());
    }

    @RequestMapping(value = MOVIE_URL + "/{movieId}", method = RequestMethod.DELETE)
    public @ResponseBody String deleteMovie(@PathVariable Long movieId)
            throws Exception {
        movieService.remove(movieId);
        return "{\"isDeleted\": \"true\"}";
    }
}
