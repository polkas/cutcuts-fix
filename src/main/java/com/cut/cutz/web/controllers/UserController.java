package com.cut.cutz.web.controllers;

import com.cut.cutz.exception.UserAuthenticationException;
import com.cut.cutz.service.IntegrationService;
import com.cut.cutz.service.UserService;
import com.cut.cutz.web.base.BaseRestController;
import com.cut.cutz.web.dto.FollowerDto;
import com.cut.cutz.web.dto.LoginDto;
import com.cut.cutz.web.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

import static com.cut.cutz.constants.RestConstants.*;


/**
 * @author Pogorelov on 08.09.2015
 */
@RestController
public class UserController extends BaseRestController {

    @Autowired
    private UserService userService;
    @Autowired
    private IntegrationService integrationService;


    @RequestMapping(value = LOGIN_URL, method = RequestMethod.GET)
    public @ResponseBody LoginDto restLogin(@RequestParam(EMAIL_PARAM) String email,
                       @RequestParam(PASSWORD_PARAM) String password,
                       HttpServletRequest req, HttpServletResponse res) throws UserAuthenticationException {
        return integrationService.userInformationOnLogin(email, password, req);
    }

    @RequestMapping(value = FACEBOOK_LOGIN_URL, method = RequestMethod.GET)
    public @ResponseBody LoginDto restFacebookLogin(@RequestParam(USERNAME_PARAM) String username,
                               @RequestParam(PHOTO_PARAM) String photoUrl, @RequestParam(EMAIL_PARAM) String email,
                               HttpServletRequest req, HttpServletResponse res) throws UserAuthenticationException {
        return integrationService.userInformationOnFacebookLogin(username, photoUrl, email, req);
    }

    @RequestMapping(value = USERS_URL + SELF_USER + STORE_INIT_DATA_URL, method = RequestMethod.GET)
    public LoginDto storeInitialData(@RequestParam(SELECTED_GENRES_PARAM) String selectedIDsComaSeparated,
                                     @RequestParam(SELECTED_USERS_PARAM) String followingUserIDsComaSeparated) {
        return integrationService.storeInitData(selectedIDsComaSeparated, followingUserIDsComaSeparated);
    }

    @RequestMapping(value = USERS_URL + "/{userId}", method = RequestMethod.GET)
    public @ResponseBody UserDto getUser(@PathVariable Integer userId) {
        return userService.getUserDtoById(userId);
    }

    @RequestMapping(value = USERS_URL + SEARCH_URL, method = RequestMethod.GET)
    public @ResponseBody List<UserDto> searchUsers(@RequestParam(QUERY_PARAM) String query,
                              HttpServletRequest req, HttpServletResponse res) {
        return userService.searchUsersDtoByQuery(query);
    }

    @RequestMapping(value = USERS_URL + "/{userId}" + FOLLOWERS_URL, method = RequestMethod.GET)
    public @ResponseBody Set<FollowerDto> getFollowers(@PathVariable Integer userId,
                                                   HttpServletRequest req, HttpServletResponse res) {
        return userService.getFollowers(userId);
    }

    @RequestMapping(value = USERS_URL + "/{userId}" + FOLLOWING_URL, method = RequestMethod.GET)
    public @ResponseBody Set<FollowerDto> getFollowing(@PathVariable Integer userId,
                                  HttpServletRequest req, HttpServletResponse res) {
        return userService.getFollowing(userId);
    }

    @RequestMapping(value = RECOVER_PASSWORD_URL, method = RequestMethod.POST)
    public void resetPassword(@RequestParam(EMAIL_PARAM) String email,
                              HttpServletRequest req, HttpServletResponse res) {
        userService.passwordReset(email);
    }

    @RequestMapping(value = USERS_URL + SELF_USER + UPDATE_USERDATA_URL, method = RequestMethod.POST, consumes = {"multipart/mixed", "multipart/form-data"})
    public UserDto updateUserData(@RequestParam(value = USERNAME_PARAM, required = false) String username,
                                  @RequestParam(value = EMAIL_PARAM, required = false) String email,
                                  @RequestParam(value = PHOTO_PARAM, required = false) MultipartFile photoFile,
                                  @RequestParam(value = PASSWORD_PARAM, required = false) String newPassword,
                                  @RequestParam(value = OLD_PASSWORD_PARAM, required = false) String oldPassword) throws Exception {
        return userService.updateUserData(username, email, photoFile, newPassword, oldPassword);
    }

    @RequestMapping(value = USERS_URL + "/{userId}" + FOLLOW_USER_URL, method = RequestMethod.POST)
    public UserDto followUser(@PathVariable Integer userId, HttpServletRequest req, HttpServletResponse res) {
        return userService.followUser(userId);
    }

}
