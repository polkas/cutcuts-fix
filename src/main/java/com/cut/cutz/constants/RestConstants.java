package com.cut.cutz.constants;

/**
 * @author Pogorelov on 08.09.2015
 */
public class RestConstants {

    //-- URLs --
    public static final String BASE_URL = "/index.html";
    public static final String STORAGE_URL = "/storage";
    public static final String API_URL = "/api";
    public static final String REST_URL = "/rest";
    public static final String SELF_USER = "/self";

    //GROUP URLs
    public static final String USERS_URL = "/users";
    public static final String MOVIE_URL = "/movie";
    public static final String GENRE_URL = "/genre";


    public static final String LOGIN_URL = "/login";
    public static final String FACEBOOK_LOGIN_URL = "/facebook/login";
    public static final String LOGOUT_URL = "/logout";
    public static final String RECOVER_PASSWORD_URL = "/password_recovery";
    public static final String UPDATE_USERDATA_URL = "/update";
    public static final String FOLLOW_USER_URL = "/follow";
    public static final String UNFOLLOW_USER_URL = "/unfollow";
    public static final String GET_USER_URL = "/get_user";
    public static final String POPULAR_URL = "/popular";
    public static final String COMMENTS_URL = "/comments";
    public static final String FEEDS_URL = "/feeds";
    public static final String NEWSET_URL = "/newest";
    public static final String STORE_INIT_DATA_URL = "/store_init_data";

    //Movies URLs
    public static final String ALL_USER_MOVIES_URL = "/all_user_movies";
    public static final String PREVIEW_MOVIE_URL = "/preview_movie";
    public static final String VIEWS_URL = "/views";
    public static final String LIKES_URL = "/likes";
    public static final String PREVIEW_COMPONENTS_URL = "/preview_components";
    public static final String MERGE_RECORDS_URL = "/merge_records";
    public static final String UPLOAD_URL = "/upload";
    public static final String UPDATE_URL = "/update";
    public static final String SEARCH_URL = "/search";
    public static final String FOLLOWING_URL = "/following";
    public static final String FOLLOWERS_URL = "/followers";
    public static final String EDIT_URL = "/edit";

    //-- URLs --

    //Dev URLs
    public static final String DEVELOPMENT_GROUP = "/dev";

    //Parameters
    public static final String EMAIL_PARAM = "email";
    public static final String USERNAME_PARAM = "username";
    public static final String PASSWORD_PARAM = "password";
    public static final String OLD_PASSWORD_PARAM = "oldPassword";
    public static final String USER_ID_PARAM = "userId";
    public static final String SELECTED_GENRES_PARAM = "selected_genres";
    public static final String SELECTED_USERS_PARAM = "selected_users";
    public static final String MOVIE_ID_PARAM = "movie_id";
    public static final String QUERY_PARAM = "query";
    public static final String COMMENT_PARAM = "comment";
    public static final String COMMENT_ID_PARAM = "comment_id";
    public static final String PHOTO_PARAM = "photo";
    public static final String THEME_PARAM = "theme";
}

