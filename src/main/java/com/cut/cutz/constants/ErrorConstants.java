package com.cut.cutz.constants;

/**
 * @author Pogorelov on 1/2/2016
 */
public class ErrorConstants {

    public static final String UNEXPECTED_USER = "Unexpected user.";
    public static final String USER_DOES_NOT_EXIST = "User does not exist.";
    public static final String UNEXPECTED_MOVIE = "Unexpected movie.";
    public static final String COMMENT_REQUIRED = "Comment is required.";
    public static final String UNEXPECTED_GENRE_ID = "Unexpected genreId.";
    public static final String UNEXPECTED_NUMBR_SEGMENTS = "Unexpected the number of segments.";
    public static final String UNEXPECTED_START_END_TIME_SEGMENT = "Unexpected start or end of the segment.";
    public static final String BAD_CREDENTIALS = "Bad credentials.";
}
