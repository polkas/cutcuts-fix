package com.cut.cutz.constants;

/**
 * @author Pogorelov on 11.09.2015
 */
public class Constants {

    //JSON properties
    public static final String USER_JSON = "user";
    public static final String USER_MOVIES_JSON = "feeds";
    public static final String MYFEED_MOVIES_JSON = "myfeed";
    public static final String NEWEST_MOVIES_JSON = "newest";
    public static final String POPULAR_MOVIES_JSON = "popular";
    public static final String GENRES_JSON = "genres";
    public static final String SUGGESTED_USERS_JSON = "suggested_users";

    //Folders path
    public static final String STORAGE_FOLDER_PATH = "storage/";
    public static final String PHOTOS_FOLDER_PATH = "photos/";
    public static final String MOVIES_FOLDER_PATH = "movies/";
    public static final String SOUND_TEMP_FOLDER_PATH = "soundtemp/";
//    public static final String POSTERS_FOLDER_PATH = STORAGE_FOLDER_PATH + "posters/";
//    public static final String GENRES_FOLDER_PATH = STORAGE_FOLDER_PATH + "genres/";

    //Extensions
    public static final String PNG_EXTENSION = ".png";

    //Default
    public static final String DEFAULT_USER_PHOTO = "defaultPhoto" + PNG_EXTENSION;

    //Email patterns
    public static final String EMAIL_DEFAULT_USER_NAME = "user";
    public static final String EMAIL_HEADER_DEAR_TEMPLATE = "Dear %s,\r\n\r\n\r\n";

    public static final String EMAIL_USER_PASSWORD_RECOVERY = "Congratulations, your password has been successfully restored for CutCutZ Application.\r\n\r\n" +
            "New password: %s\r\n\r\n" +
            "Please, use your credentials to log in.";

    public static final String EMAIL_USER_SUBJECT = "Cutcuts Account Recovery";
}
