package com.cut.cutz.db.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * @author Pogorelov on 30.09.2015
 */
@Entity(name = "segment")
public class Segment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "segment_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;

    @Column(name = "start_time", nullable = false)
    private Long startTime;

    @Column(name = "end_time", nullable = false)
    private Long endTime;

    @Column(name = "actor_nickname", nullable = false)
    private String actorNickname;

    @Column(name = "actor_picture_url", nullable = false)
    private String actorPictureUrl;

    public Long getId() {
        return id;
    }

    public Segment setId(Long id) {
        this.id = id;
        return this;
    }

    public Movie getMovie() {
        return movie;
    }

    public Segment setMovie(Movie movie) {
        this.movie = movie;
        return this;
    }

    public Long getStartTime() {
        return startTime;
    }

    public Segment setStartTime(Long startTime) {
        this.startTime = startTime;
        return this;
    }

    public Long getEndTime() {
        return endTime;
    }

    public Segment setEndTime(Long endTime) {
        this.endTime = endTime;
        return this;
    }

    public String getActorNickname() {
        return actorNickname;
    }

    public Segment setActorNickname(String actorNickname) {
        this.actorNickname = actorNickname;
        return this;
    }

    public String getActorPictureUrl() {
        return actorPictureUrl;
    }

    public Segment setActorPictureUrl(String actorPictureUrl) {
        this.actorPictureUrl = actorPictureUrl;
        return this;
    }
}
