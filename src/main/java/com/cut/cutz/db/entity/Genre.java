package com.cut.cutz.db.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author Pogorelov on 13.09.2015
 */
@Entity(name = "genre")
public class Genre implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "genre_id")
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "url", nullable = false)
    private String url;

    @OneToMany(mappedBy = "genre")
    private List<Movie> movieList;

    public Integer getId() {
        return id;
    }

    public Genre setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Genre setName(String name) {
        this.name = name;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Genre setUrl(String url) {
        this.url = url;
        return this;
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

    public Genre setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return Objects.equals(id, genre.id) &&
                Objects.equals(name, genre.name) &&
                Objects.equals(url, genre.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, url);
    }
}
