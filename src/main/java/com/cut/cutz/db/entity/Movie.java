package com.cut.cutz.db.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Pogorelov on 30.09.2015
 */
@Entity(name = "movie")
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "genre_id", nullable = false)
    private Genre genre;

    @Column(nullable = false)
    private String name;

    @Column(name = "file_name", nullable = false)
    private String fileName;

    @ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    private User author;

    @Column(name = "views")
    private Integer views;

    @ManyToMany
    @JoinTable(name = "like_table",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private List<User> likes;

    @Column(name = "date")
    private Date date;

    @Column(name = "description")
    private String description;

    @Column(name = "movie_url", nullable = false)
    private String movieUrl;

    @Column(name = "poster_url", nullable = false)
    private String posterUrl;

    @Column(name = "duration", nullable = false)
    private Integer duration;

    @OneToMany(mappedBy = "movie")
    private List<Segment> segments;

    @ManyToOne
    @JoinColumn(name = "parent")
    private Movie parent;

    @OneToMany(mappedBy="parent")
    private List<Movie> children;

    @OneToMany(mappedBy = "movie")
    private List<Comment> comments;


    public Long getId() {
        return id;
    }

    public Movie setId(Long id) {
        this.id = id;
        return this;
    }

    public Genre getGenre() {
        return genre;
    }

    public Movie setGenre(Genre genre) {
        this.genre = genre;
        return this;
    }

    public String getName() {
        return name;
    }

    public Movie setName(String name) {
        this.name = name;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public Movie setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public User getAuthor() {
        return author;
    }

    public Movie setAuthor(User author) {
        this.author = author;
        return this;
    }

    public Integer getViews() {
        if(this.views == null) views = 0;
        return views;
    }

    public Movie setViews(Integer views) {
        this.views = views;
        return this;
    }

    public List<User> getLikes() {
        return likes;
    }

    public List<Integer> getLikeUserIds() {
        return this.likes.stream().map(User::getId).collect(Collectors.toList());
    }

    public Movie setLikes(List<User> likes) {
        this.likes = likes;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public Movie setDate(Date date) {
        this.date = date;
        return this;
    }

    public String getDescription() {
        return description == null ? "" : description;
    }

    public Movie setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getMovieUrl() {
        return movieUrl;
    }

    public Movie setMovieUrl(String movieUrl) {
        this.movieUrl = movieUrl;
        return this;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public Movie setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
        return this;
    }

    public Integer getDuration() {
        return duration;
    }

    public Movie setDuration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public Movie setSegments(List<Segment> segments) {
        this.segments = segments;
        return this;
    }

    public Movie getParent() {
        return parent;
    }

    public Movie setParent(Movie parent) {
        this.parent = parent;
        return this;
    }

    public List<Movie> getChildren() {
        return children;
    }

    public Movie setChildren(List<Movie> children) {
        this.children = children;
        return this;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public Movie setComments(List<Comment> comments) {
        this.comments = comments;
        return this;
    }
}
