package com.cut.cutz.db.entity;

import com.cut.cutz.helper.DataHelper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Pogorelov on 09.09.2015
 */
@Entity(name = "user")
public class User implements Serializable, UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer id;

    @Column
    private String name;

    @Column
    private String password;

    @Column(nullable = false)
    private String email;

    @Column(name = "photo_url", nullable = false)
    private String photoUrl;

    @ManyToMany
    @JoinTable(name = "user_genre",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")}
    )
    private List<Genre> genres;

    @OneToMany(mappedBy = "author")
    private List<Movie> movies;

    @ManyToMany(cascade = CascadeType.REMOVE)
    @JoinTable(name = "user_followers",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_follower_id")})
    private Set<User> followers = new HashSet<>();

    @ManyToMany(cascade = CascadeType.REMOVE)
    @JoinTable(name = "user_followers",
            joinColumns = {@JoinColumn(name = "user_follower_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private Set<User> following = new HashSet<>();

    @OneToMany(mappedBy = "author")
    private List<Comment> comments;

    @ManyToMany
    @JoinTable(name = "like_table",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private List<Movie> likes;

    public Integer getId() {
        return id;
    }

    public User setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public List<Genre> getGenres() {
        if(genres == null) genres = new ArrayList<>();
        return genres;
    }

    public User setGenres(List<Genre> genres) {
        this.genres = genres;
        return this;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public User setMovies(List<Movie> movies) {
        this.movies = movies;
        return this;
    }

    public List<Movie> getCretedMovies() {
        return this.movies.stream().filter(movie -> movie.getParent() != null).collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = DataHelper.encodedValue(password);
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public User setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
        return this;
    }

    public Set<User> getFollowers() {
        return followers;
    }

    public User setFollowers(Set<User> followers) {
        this.followers = followers;
        return this;
    }

    public Set<User> getFollowing() {
        return following;
    }

    public User setFollowing(Set<User> following) {
        this.following = following;
        return this;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public User setComments(List<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public List<Movie> getLikes() {
        return likes;
    }

    public User setLikes(List<Movie> likes) {
        this.likes = likes;
        return this;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> auths = new ArrayList<>();

        auths.add(new SimpleGrantedAuthority("USER"));

        return auths;
    }

    @Override
    public String getUsername() {
        return this.name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(name, user.name) &&
                Objects.equals(password, user.password) &&
                Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, password, email);
    }
}
