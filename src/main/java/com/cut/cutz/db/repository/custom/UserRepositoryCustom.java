package com.cut.cutz.db.repository.custom;

import com.cut.cutz.db.entity.User;

import java.util.List;

/**
 * @author Pogorelov on 11.09.2015
 */
public interface UserRepositoryCustom {

    Boolean followUser(User currentUser, Integer userId);

    List<User> findByNameLike(Integer currentUserId, String query);

    List<User> findSuggestedUsersByTotalLikesOfMovies(User currentUser, int count);
}
