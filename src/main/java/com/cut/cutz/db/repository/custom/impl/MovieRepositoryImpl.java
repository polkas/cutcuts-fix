package com.cut.cutz.db.repository.custom.impl;


import com.cut.cutz.constants.ErrorConstants;
import com.cut.cutz.db.entity.Movie;
import com.cut.cutz.db.entity.QMovie;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.db.repository.custom.MovieRepositoryCustom;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Pogorelov on 11.09.2015
 */
public class MovieRepositoryImpl extends BaseJpaRepository implements MovieRepositoryCustom {

    @Override
    public List<Movie> getAllMoviesByUserId(Integer userId) {
        QMovie qMovie = QMovie.movie;
        return from(qMovie).where(qMovie.author.id.eq(userId)
                .and(qMovie.parent.isNotNull())).list(qMovie);
    }

    @Override
    public List<Movie> getNewestMovies() {
        QMovie qMovie = QMovie.movie;
        List<Movie> list = from(qMovie)
                .where(qMovie.parent.isNotNull())
                .orderBy(qMovie.date.desc())
                .list(qMovie);

        return list;
    }

    @Override
    public List<Movie> getPopularMovies() {
        QMovie qMovie = QMovie.movie;
        List<Movie> list = from(qMovie)
                .where(qMovie.parent.isNotNull())
                .orderBy(qMovie.views.desc())
                .list(qMovie);

        return list;
    }

    @Override
    public List<Movie> getLastFriendsMoviesWithNumOfMovies(Integer userId) {
        QMovie qMovie = QMovie.movie;

        if (userId == null) {
            return null;
        }

        User currentUser;

        try {
            currentUser = entityManager.find(User.class, userId);
        } catch (NoResultException e) {
            currentUser = null;
        }

        if (currentUser == null) {
            return null;
        }

        List<Integer> friendsUserId = currentUser.getFollowing().stream().map(User::getId).collect(Collectors.toList());

        friendsUserId.add(userId);

        List<Movie> list = from(qMovie)
                .where(qMovie.parent.isNotNull().and(qMovie.author.id.in(friendsUserId)))
                .orderBy(qMovie.date.desc())
                .list(qMovie);

        return list;
    }

    @Override
    public List<Movie> findSimilarMovies(Long movieId, Long parentId) {
        QMovie qMovie = QMovie.movie;

        List<Movie> list = from(qMovie)
                .where(qMovie.parent.id.eq(parentId)
                        .and(qMovie.id.ne(movieId)))
                .orderBy(qMovie.date.desc())
                .list(qMovie);
        return list;
    }

    @Override
    public List<Movie> getEditableMoviesWithNumOfMovies(Integer genreId) {
        QMovie qMovie = QMovie.movie;
        List<Movie> list = from(qMovie)
                .where(qMovie.parent.isNull().and(qMovie.genre.id.eq(genreId)))
                .orderBy(qMovie.date.desc())
                .list(qMovie);

        return list;
    }

    @Override
    @Transactional
    public Boolean changeStatusMovieLike(Long movieId, Integer currentUserId) {
        Movie movieDB = entityManager.find(Movie.class, movieId);
        User userDB = entityManager.find(User.class, currentUserId);

        if (userDB == null) {
            throw new IllegalArgumentException(ErrorConstants.UNEXPECTED_USER);
        }

        if (movieDB == null) {
            throw new IllegalArgumentException(ErrorConstants.UNEXPECTED_MOVIE);
        }

        Boolean isLiked;

        if (!movieDB.getLikes().contains(userDB)) {
            entityManager.createNativeQuery("INSERT INTO like_table (movie_id, user_id) VALUES (?, ?)")
                    .setParameter(1, movieId)
                    .setParameter(2, currentUserId)
                    .executeUpdate();

            isLiked = true;
        } else {
            entityManager.createNativeQuery("DELETE FROM like_table WHERE movie_id=? AND user_id=?")
                    .setParameter(1, movieId)
                    .setParameter(2, currentUserId)
                    .executeUpdate();

            isLiked = false;
        }

        return isLiked;
    }

    @Override
    @Transactional
    public Integer getLikesCountByMovie(Long movieId) {
        return entityManager.find(Movie.class, movieId).getLikes().size();
    }
}
