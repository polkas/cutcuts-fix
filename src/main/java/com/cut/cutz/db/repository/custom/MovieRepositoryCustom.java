package com.cut.cutz.db.repository.custom;

import com.cut.cutz.db.entity.Movie;

import java.util.List;

/**
 * @author Pogorelov on 11.09.2015
 */
public interface MovieRepositoryCustom {

    List<Movie> getAllMoviesByUserId(Integer userId);

    List<Movie> getNewestMovies();

    List<Movie> getPopularMovies();

    List<Movie> getLastFriendsMoviesWithNumOfMovies(Integer userId);

    List<Movie> getEditableMoviesWithNumOfMovies(Integer genreId);

    List<Movie> findSimilarMovies(Long movieId, Long parentId);

    Boolean changeStatusMovieLike(Long movieId, Integer currentUserId);

    Integer getLikesCountByMovie(Long movieId);

//    List<Movie> find(String name);
}
