package com.cut.cutz.db.repository.custom.impl;

import com.cut.cutz.db.entity.Comment;
import com.cut.cutz.db.entity.QComment;
import com.cut.cutz.db.repository.custom.CommentRepositoryCustom;

import java.util.List;

/**
 * @author Pogorelov on 13.09.2015
 */

public class CommentRepositoryImpl extends BaseJpaRepository implements CommentRepositoryCustom {

    @Override
    public List<Comment> findCommentsForMovieId(Long movieId) {
        QComment qComment = QComment.comment;
        return from(qComment).where(qComment.movie.id.eq(movieId)).list(qComment);
    }
}
