package com.cut.cutz.db.repository;

import com.cut.cutz.db.entity.User;
import com.cut.cutz.db.repository.custom.UserRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Pogorelov on 09.09.2015
 */
public interface UserRepository extends JpaRepository<User, Integer>, UserRepositoryCustom {

    User findByEmail(String email);

    User findByName(String name);

}
