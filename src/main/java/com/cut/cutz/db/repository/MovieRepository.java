package com.cut.cutz.db.repository;

import com.cut.cutz.db.entity.Movie;
import com.cut.cutz.db.repository.custom.MovieRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Pogorelov on 09.09.2015
 */
public interface MovieRepository extends JpaRepository<Movie, Long>, MovieRepositoryCustom {

    Movie findById(Long id);

    List<Movie> findByNameLikeAndParentIdIsNotNull(String name);

    List<Movie> findByNameLike(String name);

}
