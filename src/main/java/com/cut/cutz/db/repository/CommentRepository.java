package com.cut.cutz.db.repository;

import com.cut.cutz.db.entity.Comment;
import com.cut.cutz.db.repository.custom.CommentRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author Pogorelov on 13.09.2015
 */
public interface CommentRepository extends JpaRepository<Comment, Long>, CommentRepositoryCustom {

}
