package com.cut.cutz.db.repository.custom.impl;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.EntityPath;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;

/**
 * @author Pogorelov on 25.09.2015
 */
public abstract class BaseJpaRepository<T> {

    @PersistenceContext(unitName = "default")
    protected EntityManager entityManager;

    protected JPAQuery from(EntityPath entity) {
        return new JPAQuery(entityManager).from(entity);
    }

    protected <T> List<T> safe(List<T> list) {
        return list == null ? Collections.<T>emptyList() : list;
    }

    protected JPAUpdateClause update(EntityPath entityPath) {
        return new JPAUpdateClause(entityManager, entityPath);
    }
}
