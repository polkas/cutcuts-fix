package com.cut.cutz.db.repository.custom.impl;

import com.cut.cutz.constants.ErrorConstants;
import com.cut.cutz.db.entity.QUser;
import com.cut.cutz.db.entity.User;
import com.cut.cutz.db.repository.custom.UserRepositoryCustom;
import com.cut.cutz.exception.UnexpectedDataException;
import com.mysema.query.types.expr.BooleanExpression;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Pogorelov on 11.09.2015
 */
public class UserRepositoryImpl extends BaseJpaRepository implements UserRepositoryCustom {

    @Override
    @Transactional
    public Boolean followUser(User currentUser, Integer userId) {
        User currentUserDB = entityManager.find(User.class, currentUser.getId());
        User userDB = entityManager.find(User.class, userId);

        boolean hasRelations = false;

        if (!currentUserDB.getFollowing().contains(userDB) && !currentUserDB.getId().equals(userDB.getId())) {
            entityManager.createNativeQuery("INSERT INTO user_followers (user_id, user_follower_id) VALUES (?, ?)")
                    .setParameter(1, userId)
                    .setParameter(2, currentUserDB.getId())
                    .executeUpdate();

            hasRelations = true;
        } else if (currentUserDB.getFollowing().contains(userDB)) {
            if (currentUserDB.getFollowing().contains(userDB)) {
                entityManager.createNativeQuery("DELETE FROM user_followers WHERE user_id=? AND user_follower_id=?")
                        .setParameter(1, userDB)
                        .setParameter(2, currentUserDB.getId())
                        .executeUpdate();

                hasRelations = false;
            }
        }

        return hasRelations;
    }

        @Override
        public List<User> findByNameLike (Integer currentUserId, String query){
            QUser qUser = QUser.user;
            return from(qUser).where(qUser.name.like("%" + query + "%").and(qUser.id.ne(currentUserId))).list(qUser);
        }

        @Override
        @Transactional
        public List<User> findSuggestedUsersByTotalLikesOfMovies (User currentUser,int count){
            if (currentUser == null) {
                throw new UnexpectedDataException(ErrorConstants.UNEXPECTED_USER);
            }

            QUser qUser = QUser.user;

            @SuppressWarnings("unchecked")
            List<Integer> alreadyFollowingIds = safe(from(qUser).where(qUser.id.eq(currentUser.getId()))
                    .list(qUser.following.any().id));

            //don't add yourself to suggestions
            alreadyFollowingIds.add(currentUser.getId());

            BooleanExpression booleanExpression = null;
            if (alreadyFollowingIds.size() > 0) {
                booleanExpression = qUser.id.notIn(alreadyFollowingIds);
            }
            List<Integer> userIds = from(qUser).where(booleanExpression).list(qUser.id);

            Map<Integer, Integer> userLikesMap = new HashMap<>();

            userIds.forEach(id -> {
                User user = from(qUser).where(qUser.id.eq(id)).singleResult(qUser);

                if (user != null && user.getLikes() != null) {
                    Integer likes = user.getLikes().size();

                    if (likes > 0) {
                        userLikesMap.put(id, likes);
                    }
                }
            });

            List<Integer> suggestedUsersIds = userLikesMap.entrySet()
                    .stream()
                    .sorted((v1, v2) -> Integer.compare(v2.getValue(), v1.getValue()))
                    .map(Map.Entry::getKey)
                    .limit(count)
                    .collect(Collectors.toList());

            List<User> suggestedUsers = new ArrayList<>();

            suggestedUsersIds.stream().forEach(userId -> {
                User suggestedUser = from(qUser).where(qUser.id.eq(userId)).singleResult(qUser);
                if (suggestedUser != null) {
                    suggestedUsers.add(suggestedUser);
                }
            });

            return suggestedUsers;
        }
    }
