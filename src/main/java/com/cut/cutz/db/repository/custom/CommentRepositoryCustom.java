package com.cut.cutz.db.repository.custom;

import com.cut.cutz.db.entity.Comment;

import java.util.List;

/**
 * @author Pogorelov on 13.09.2015
 */
public interface CommentRepositoryCustom {

    List<Comment> findCommentsForMovieId(Long movieId);
}
