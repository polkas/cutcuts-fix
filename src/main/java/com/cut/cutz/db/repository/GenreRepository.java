package com.cut.cutz.db.repository;

import com.cut.cutz.db.entity.Genre;
import com.cut.cutz.db.repository.custom.GenreRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Pogorelov on 13.09.2015
 */
public interface GenreRepository extends JpaRepository<Genre, Integer>, GenreRepositoryCustom {

    List<Genre> findAll();
}
