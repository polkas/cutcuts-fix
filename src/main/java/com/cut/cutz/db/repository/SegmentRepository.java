package com.cut.cutz.db.repository;

import com.cut.cutz.db.entity.Segment;
import com.cut.cutz.db.repository.custom.SegmentRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Pogorelov on 09.09.2015
 */
public interface SegmentRepository extends JpaRepository<Segment, Long>, SegmentRepositoryCustom {

}
