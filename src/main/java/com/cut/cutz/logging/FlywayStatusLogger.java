package com.cut.cutz.logging;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.internal.info.MigrationInfoDumper;
import org.slf4j.Logger;

/**
 * @author Pogorelov on 11.09.2015
 */
public class FlywayStatusLogger {

    @InjectLogger
    private Logger log;

    private final Flyway flyway;

    public FlywayStatusLogger(Flyway flyway) {
        this.flyway = flyway;
    }

    public void logStatus(){
        log.info("\n" + MigrationInfoDumper.dumpToAsciiTable(flyway.info().all()));
    }
}
