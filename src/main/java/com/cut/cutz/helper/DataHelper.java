package com.cut.cutz.helper;

import com.cut.cutz.db.entity.User;
import com.cut.cutz.exception.UserAuthenticationException;
import org.hibernate.internal.util.compare.EqualsHelper;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * @author Pogorelov on 11.09.2015
 */
public class DataHelper {


    public static String encodedValue(String value) {
        if(value == null) return null;

        ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder();
        final String result = shaPasswordEncoder.encodePassword(value, "L|QWr_!=+`");
        return result;
    }

    public static boolean isValidPassword(String password, User user) {
        return EqualsHelper.equals(encodedValue(password), user.getPassword());
    }

    public static void emailValidation(String email) throws UserAuthenticationException {
        final String regex = "^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)";
        final boolean isValid = email.matches(regex);
        if (!isValid) {
            final String msg = "Email is incorrect!";
            throw new UserAuthenticationException(msg);
        }
    }

    public static void usernameValidation(String username) {
        final String regex = "[A-Za-z]+([\\s|-][A-Za-z]*+)+";

        final boolean isValid = username.matches(regex);
        if (!isValid) {
            final String msg = "Username is incorrect!";
            throw new UserAuthenticationException(msg);
        }
    }

    public static String extractUserNameFromEmail(String email) {
        return email.replaceAll("@.+", "");
    }

    public static String generateRandomUUID() {
        return UUID.randomUUID().toString();
    }

    public static <T> List<T> safe(List<T> list) {
        return list == null ? Collections.emptyList() : list;
    }
}