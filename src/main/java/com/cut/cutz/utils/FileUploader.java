package com.cut.cutz.utils;

import com.cut.cutz.constants.Constants;
import com.cut.cutz.helper.DataHelper;
import com.cut.cutz.logging.InjectLogger;
import com.cut.cutz.manager.StorageManager;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author Pogorelov on 01.11.2015
 */
@Service
public class FileUploader {

    @Autowired
    private StorageManager storageManager;

    @InjectLogger
    private Logger log;

    public String saveFile(MultipartFile pic, LogInfo logInfo) throws IOException {
        String picUrl = "";

        if (pic == null || pic.isEmpty()) {
            return picUrl;
        }

        final String folder = storageManager.getPhotosStorage();
        final String fileName = DataHelper.generateRandomUUID() + Constants.PNG_EXTENSION;

        log.info("Start %s processing.", logInfo);

        if (!saveFile(pic, folder, fileName)) {
            log.error("Failed to save %s.", logInfo);
            return picUrl;
        }

        log.info("%s is saved: %s", logInfo, fileName);

        picUrl = storageManager.getFullPhotoUrl(fileName);

        return picUrl;
    }

    private boolean saveFile(MultipartFile multipartFile, final String folder, final String fileName) throws IOException {

        if(multipartFile == null || multipartFile.isEmpty()) return false;

        File file = new File(folder + fileName);

        if(!file.exists()) file.createNewFile();

        final byte[] bytes = multipartFile.getBytes();
        FileUtils.writeByteArrayToFile(file, bytes);

        return true;
    }
}
