package com.cut.cutz.utils;

/**
 * @author Pogorelov on 8/31/2016
 */
public enum LogInfo {
    POSTER("Poster"),
    USER_PHOTO("User photo"),
    ACTOR_PHOTO("Actor photo");

    private String value;

    LogInfo(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
