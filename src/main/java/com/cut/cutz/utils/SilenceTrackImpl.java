package com.cut.cutz.utils;

import com.coremedia.iso.boxes.CompositionTimeToSample;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import com.coremedia.iso.boxes.SubSampleInformationBox;
import com.googlecode.mp4parser.authoring.*;
import com.googlecode.mp4parser.boxes.mp4.samplegrouping.GroupEntry;
import com.googlecode.mp4parser.util.CastUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Pogorelov on 8/15/2016
 */
public class SilenceTrackImpl implements Track {
    Track source;
    List<Sample> samples = new LinkedList();
    long[] decodingTimes;
    String name;

    public SilenceTrackImpl(Track ofType, long ms) {
        this.source = ofType;
        this.name = ms + "ms silence";
        int numFrames = CastUtils.l2i(this.getTrackMetaData().getTimescale() * ms / 1000L / 1024L);
        this.decodingTimes = new long[numFrames];
        Arrays.fill(this.decodingTimes, this.getTrackMetaData().getTimescale() * ms / (long) numFrames / 1000L);

        Sample sample = ofType.getSamples().get(0);
        while (numFrames-- > 0) {
            this.samples.add(sample);
        }

    }

    public void close() throws IOException {
    }

    public SampleDescriptionBox getSampleDescriptionBox() {
        return this.source.getSampleDescriptionBox();
    }

    public long[] getSampleDurations() {
        return this.decodingTimes;
    }

    public long getDuration() {
        long duration = 0L;
        long[] var3 = this.decodingTimes;
        int var4 = this.decodingTimes.length;

        for (int var5 = 0; var5 < var4; ++var5) {
            long delta = var3[var5];
            duration += delta;
        }

        return duration;
    }

    public TrackMetaData getTrackMetaData() {
        return this.source.getTrackMetaData();
    }

    public String getHandler() {
        return this.source.getHandler();
    }

    public List<Sample> getSamples() {
        return this.samples;
    }

    public SubSampleInformationBox getSubsampleInformationBox() {
        return null;
    }

    public List<CompositionTimeToSample.Entry> getCompositionTimeEntries() {
        return null;
    }

    public long[] getSyncSamples() {
        return null;
    }

    public List<com.coremedia.iso.boxes.SampleDependencyTypeBox.Entry> getSampleDependencies() {
        return null;
    }

    public String getName() {
        return this.name;
    }

    public List<Edit> getEdits() {
        return null;
    }

    public Map<GroupEntry, long[]> getSampleGroups() {
        return this.source.getSampleGroups();
    }
}

